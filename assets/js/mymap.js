var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -33.7839, lng: 151.2672},
        zoom: 14
    });
    var request = {
        location: map.getCenter(),
		radius: '500',
		query: 'Paradise Beach Purveyors'
	};
	var service = new google.maps.places.PlacesService(map);
	service.textSearch(request, callback);
}

function callback(results, status) 
{
	if (status == google.maps.places.PlacesServiceStatus.OK) 
    {
        var placeId = results[0].place_id;
		var service = new google.maps.places.PlacesService(map);
                    
		service.getDetails({placeId: placeId}, function(place, status) 
		{
			if (status === google.maps.places.PlacesServiceStatus.OK) 
			{
                var marker = new google.maps.Marker({map: map, position: place.geometry.location});
                map.setCenter(place.geometry.location);
                var infowindow = new google.maps.InfoWindow();
                google.maps.event.addListener(marker, 'click', function() 
                {
					infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
									   place.formatted_address + '</div>');
					infowindow.open(map, this);
                });
					infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
									   place.formatted_address + '</div>');
					infowindow.open(map,marker);
			}
		});
	}
}