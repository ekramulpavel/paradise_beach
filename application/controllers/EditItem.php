<?php
/**
 * Created by PhpStorm.
 * User: ashikmahmud
 * Date: 05/09/2017
 * Time: 10:58
 */

class EditItem extends CI_Controller
{
    public function edit_product_view($id = 0)
    {
        if ($this->session->userdata('logged_in')) {
            $this->session->unset_userdata('recipe_list');
            $this->session->unset_userdata('old_recipe_list');
            $this->session->unset_userdata('award_list');
            $this->session->unset_userdata('old_award_list');
            $this->session->unset_userdata('category_list');
            $alldata = $this->Admin_Model->product_detail($id);
            $data['products'] = $alldata['products'];
            $data['recipes'] = $alldata['recipes'];
            $data['awards'] = $alldata['awards'];
            $data['category'] = $alldata['category'];
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'edit/edit_product';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }
    public function edit_testimonial_view($id = 0)
    {
        if ($this->session->userdata('logged_in')) {
            $data['testimonials'] = $this->Admin_Model->testimonial_detail($id);
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'edit/edit_testimonial';
            $this->load->view('home', $data);
        }
        else {
            redirect('home/admin_login');
        }
    }
    public function edit_testimonial()
    {
        if ($this->session->userdata('logged_in')) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $name = $this->input->post('name');
                $description = $this->input->post('description');
                $testimonial_data = array(
                    'name' => $name,
                    'description' => $description
                );
                $testimonial_file = $_FILES['upload-testimonial-image']['name'];
                if(!empty($testimonial_file))
                {
                    $config['upload_path'] = FCPATH . 'Itemimage/testimonial/';
                    $config['allowed_types'] = 'jpg|png|jpeg|gif';
                    $config['overwrite'] = false;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('upload-testimonial-image');
                    $upload_data = $this->upload->data();
                    $testimonial_image = $upload_data['file_name'];
                    $image_url = base_url() . "Itemimage/testimonial/" . $testimonial_image;
                    $testimonial_data['image'] = $testimonial_image;
                    $testimonial_data['imageurl'] = $image_url;
                    
                }
                $inserted_testimonial = $this->EditItem_Model->edit_testimonial($testimonial_data);

                if ($inserted_testimonial) {
                    redirect('admin/all_testimonial');
                }
            }
        }
         else {
            redirect('home/admin_login');
        }
    }
    public function edit_subscriber_view($id = 0)
    {
        if($this->session->userdata('logged_in'))
        {
            $data['subscriber'] = $this->EditItem_Model->get_subscriber($id);
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'edit/edit_subscriber';
            $this->load->view('home', $data);
        }
        else
        {
            redirect('home/admin_login');
        }
    }
    public function edit_home_view($id = 0)
    {
        if ($this->session->userdata('logged_in')) {
            $data['homes'] = $this->EditItem_Model->get_home($id);
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'edit/edit_home';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        } 
    }
    public function edit_recipe_view($id = 0)
    {
        if ($this->session->userdata('logged_in')) {
            $data['recipes'] = $this->EditItem_Model->get_recipe($id);
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'edit/edit_recipe';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }

    public function edit_banner_view($id = 0)
    {
        $data['banners'] = $this->EditItem_Model->get_banner($id);
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'edit/edit_banner';
        $this->load->view('home', $data);
    }

    public function edit_category_view($id = 0)
    {
        if ($this->session->userdata('logged_in')) {
            $category = $this->EditItem_Model->get_category($id);
            $data['categories'] = $category['category'];
            $data['all_category'] = $category['all_category'];
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'edit/edit_category';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }

    public function edit_award_view($id = 0)
    {
        if ($this->session->userdata('logged_in')) {
            $data['awards'] = $this->EditItem_Model->get_award($id);
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'edit/edit_award';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }
    public function edit_about_view($id = 0)
    {
        if ($this->session->userdata('logged_in')) {
            $data['about'] = $this->EditItem_Model->get_about($id);
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'edit/edit_about';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }
    public function edit_about()
    {
        if($this->session->userdata('logged_in'))
        {
            $about_description = $this->input->post('about-description');
            $about_file = $_FILES['about-image']['name'];
            $about_data = array(
                'description' => $about_description
                );
            
            if(!empty($about_file))
            {
                $config['upload_path'] = FCPATH . 'Itemimage/aboutus/';
                $config['allowed_types'] = 'jpg|png|jpeg|gif';
                $config['overwrite'] = false;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('about-image');
                $upload_data = $this->upload->data();
                $about_image = $upload_data['file_name'];
                $image_url = base_url() . "Itemimage/aboutus/" . $about_image;
                
                $about_data['image'] = $about_image;
                $about_data['imageurl'] = $image_url;
            }
            $result = $this->EditItem_Model->edit_about($about_data);
            if($result)
            {
                redirect('admin/all_about');
            }
        }
    }
    public function edit_award()
    {
        if ($this->session->userdata('logged_in')) {
            $award_date = $this->input->post('award-date');
            $award_description = $this->input->post('award-description');
            $format_awarddate = date("Y-m-d", strtotime($award_date));
            $award_data = array(
                'awardyear' => $format_awarddate,
                'description' => $award_description
            );
            $award_file = $_FILES['award-image']['name'];
            if(!empty($award_file))
            {
                $config['upload_path'] = FCPATH . 'Itemimage/award/';
                $config['allowed_types'] = 'jpg|png|jpeg|gif';
                $config['overwrite'] = false;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('award-image');
                $upload_data = $this->upload->data();
                $award_image = $upload_data['file_name'];
                $image_url = base_url() . "Itemimage/award/" . $award_image;
                
                $award_data['filename'] = $award_image;
                $award_data['imageurl'] = $image_url;
            }

            $result = $this->EditItem_Model->edit_award($award_data);
            if ($result) {
                redirect('admin/all_awards');
            }
        } else {
            redirect('home/admin_login');
        }
    }
    public function edit_home()
    {
        if ($this->session->userdata('logged_in')) {
            $home_title = $this->input->post('home-title');
            $home_subtitle = $this->input->post('home-subtitle');
            $home_link = $this->input->post('home-link');
            $home_data = array(
                'title' => $home_title,
                'subtitle' => $home_subtitle
            );
            if(!empty($home_link))
            {
               $home_data['link'] = $home_link;
            }
            $home_file = $_FILES['home-image']['name'];
            if(!empty($home_file))
            {
                $config['upload_path'] = FCPATH . 'Itemimage/home/';
                $config['allowed_types'] = 'jpg|png|jpeg|gif';
                $config['overwrite'] = false;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('home-image');
                $upload_data = $this->upload->data();
                $home_image = $upload_data['file_name'];
                $image_url = base_url() . "Itemimage/home/" . $home_image;
                
                $home_data['image'] = $home_image;
                $home_data['imageurl'] = $image_url;
            }

            $result = $this->EditItem_Model->edit_home($home_data);
            if ($result) {
                redirect('admin/all_home');
            }
        } else {
            redirect('home/admin_login');
        } 
    }
    public function edit_category()
    {
        if ($this->session->userdata('logged_in')) {
            $category_name = $this->input->post('category-name');
            $categoryid = $this->input->post('parent-category');

            $category_data = array(
                'categoryname' => $category_name
            );
            if($categoryid != 0)
            {
                $category_data['categoryid'] = $categoryid;
            }
            $result = $this->EditItem_Model->edit_category($category_data);
            if ($result) {
                redirect('admin/all_category');
            }
        } else {
            redirect('home/admin_login');
        }

    }
    public function edit_subscriber()
    {
        if($this->session->userdata('logged_in'))
        {
            $email = $this->input->post('email');
            $subscribers_data = array(
                'email' => $email
                );
            $result = $this->EditItem_Model->edit_subscriber($subscribers_data);
            if($result)
            {
                redirect('admin/all_subscribers');
            }
        }
    }
    public function edit_banner()
    {
        $banner_description = $this->input->post('banner-description');
        $banner_data = array(
            'description' => $banner_description
        );
        $banner_file = $_FILES['banner-image']['name'];
        if(!empty($banner_file))
        {
        
            $config['upload_path'] = FCPATH . 'Itemimage/banner/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['overwrite'] = false;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload('banner-image');
            $upload_data = $this->upload->data();
            $banner_image = $upload_data['file_name'];
            $image_url = base_url() . "Itemimage/banner/" . $banner_image;
            
            $banner_data['image'] = $banner_image;
            $banner_data['imageurl'] = $image_url;
        }
        $result = $this->EditItem_Model->edit_banner($banner_data);

        if ($result) {
            redirect('admin/all_banner');
        }
    }

    public function edit_recipe()
    {
        if ($this->session->userdata('logged_in')) {
            $recipe_name = $this->input->post('recipe-title');
            $recipe_ingredient = $this->input->post('recipe-ingredient');
            $recipe_procedure = $this->input->post('recipe-procedure');

            $recipe_data = array(
                'title' => $recipe_name,
                'ingredient' => $recipe_ingredient,
                'recipe_procedure' => $recipe_procedure
            );
            $recipe_file = $_FILES['recipe-image']['name'];
            
            if(!empty($recipe_file))
            {
                $config['upload_path'] = FCPATH . 'Itemimage/recipe/';
                $config['allowed_types'] = 'jpg|png|jpeg|gif';
                $config['overwrite'] = false;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('recipe-image');
                $upload_data = $this->upload->data();
                $recipe_image = $upload_data['file_name'];
                $image_url = base_url() . "Itemimage/recipe/" . $recipe_image;
                
                $recipe_data['filename'] = $recipe_image;
                $recipe_data['imageurl'] = $image_url;
            }
            $result = $this->EditItem_Model->edit_recipe($recipe_data);

            if ($result) {
                redirect('admin/all_recipes');
            }
        } else {
            redirect('home/admin_login');
        }
    }
    public function edit_contact()
    {
        if($this->session->userdata('logged_in'))
        {
            $contactno = $this->input->post('contactno');
            $address = $this->input->post('address');
            $email = $this->input->post('email');
            
            $contact_data = array(
                'contactno' => $contactno,
                'address' => $address,
                'email' => $email
            );
            $result = $this->EditItem_Model->edit_contact($contact_data);
            
            if($result)
            {
                redirect('admin/contactinfo');
            }
        }
        else
        {
            redirect('home/admin_login');
        }
    }
    public function edit_product()
    {
        if ($this->session->userdata('logged_in')) {
            $product_file_name = $_FILES['product-image']['name'];
            $nutrition_file_name = $_FILES['nutrition-image']['name'];

            $product_name = $this->input->post('product-name');
            $product_description = $this->input->post('product-description');
            $product_ingredient = $this->input->post('product-ingredient');
            $product_allergen = $this->input->post('product-allergen');

            $product_data = array(
                'productname' => $product_name,
                'description' => $product_description,
                'ingredients' => $product_ingredient,
                'allergens' => $product_allergen
            );
            if (!empty($product_file_name)) {
                //Upload Product File
                $config['upload_path'] = FCPATH . 'Itemimage/product/';
                $config['allowed_types'] = 'jpg|png|jpeg|gif';
                $config['overwrite'] = false;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('product-image');
                $upload_data = $this->upload->data();
                $product_image = $upload_data['file_name'];
                $product_image_url = base_url() . "Itemimage/product/" . $product_image;

                $product_data['productimage'] = $product_image;
                $product_data['imageurl'] = $product_image_url;
            }
            if (!empty($nutrition_file_name)) {
                //Upload Nutrition Image
                $config['upload_path'] = FCPATH . 'Itemimage/nutrition/';
                $config['allowed_types'] = 'jpg|png|jpeg|gif';
                $config['overwrite'] = false;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('nutrition-image');
                $upload_data = $this->upload->data();
                $nutrition_image = $upload_data['file_name'];
                $nutrition_image_url = base_url() . "Itemimage/nutrition/" . $nutrition_image;

                $product_data['nutritionfile'] = $nutrition_image;
                $product_data['nutritionfileurl'] = $nutrition_image_url;
            }
            $result = $this->EditItem_Model->edit_product($product_data);
            if ($result) {
                $this->session->unset_userdata('recipe_list');
                $this->session->unset_userdata('old_recipe_list');
                $this->session->unset_userdata('award_list');
                $this->session->unset_userdata('old_award_list');
                $this->session->unset_userdata('category_list');
                $this->session->unset_userdata('old_category_list');
                redirect('admin/all_products');
            }
        } else {
            redirect('home/admin_login');
        }
    }
    public function edit_store()
    {
        if($this->session->userdata('logged_in'))
        {
            $result = $this->EditItem_Model->edit_store();
            if($result)
            {
                redirect('admin/all_store');
            }
        }
        else
        {
            redirect('home/admin_login');
        } 
    }
    public function edit_store_view($id = 0)
    {
        if($this->session->userdata('logged_in'))
        {
            $data['stores'] = $this->EditItem_Model->get_store($id);
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'edit/edit_store';
            $this->load->view('home', $data);
        }
        else
        {
            redirect('home/admin_login');
        }
    }
}