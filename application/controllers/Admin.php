<?php

class Admin extends CI_Controller
{
    public function index()
    {
        if ($this->session->userdata('logged_in')) {
            $data['products'] = $this->Admin_Model->get_products();
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'all-products';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }
    public function subscribe_newsletter()
    {
        $result = $this->Admin_Model->subscribe_newsletter();
        if($result)
        {
            echo "Thanks for subscribing to newsletter!";
        }
    }
    public function login()
    {
        $this->form_validation->set_rules('useremail', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data = array(
                'errors' => validation_errors()
            );
            $this->session->set_flashdata($data);
            redirect('home/admin_login');
        } else {
            $useremail = $this->input->post('useremail');
            $password = $this->input->post('password');

            //user_model should be autoload first in autoload.php file autoload['model']=array(user_model)
            $user_data = $this->Admin_Model->login_user($useremail, $password); //this will return the user_id from the database
            $user_id = $user_data['user_id'];


            if ($user_id) {
                $userdata = array(
                    'user_id' => $user_id,
                    'useremail' => $useremail,
                    'logged_in' => true,
                );

                $this->session->set_userdata($userdata); //this session will help us to work with this data in the view
                $this->session->set_flashdata('login_success', 'You are logged in');
                redirect('admin');
            } else {
                $this->session->set_flashdata('login_failed', 'Sorry, You are not logged in');
                redirect('home/admin_login');
            }
        }
    }
    public function all_subscribers()
    {
        $data['subscribers'] = $this->Admin_Model->all_subscribers();
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'all-subscribers';
        $this->load->view('home', $data);
    }
    public function logout()
    {
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('useremail');
        $this->session->unset_userdata('logged_in');

        redirect('home/admin_login');
    }
    public function add_home()
    {
        if ($this->session->userdata('logged_in')) {
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'add-home';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }
    public function add_product()
    {
        if ($this->session->userdata('logged_in')) {
            $this->session->unset_userdata('category_list');
            $this->session->unset_userdata('recipe_list');
            $this->session->unset_userdata('award_list');
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'add-new-product';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }

    public function add_recipe()
    {
        if ($this->session->userdata('logged_in')) {
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'add-recipe';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }

    public function add_award()
    {
        if ($this->session->userdata('logged_in')) {
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'add-awards';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }

    public function add_aboutus()
    {
        if ($this->session->userdata('logged_in')) {
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'add-aboutus';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }
    public function all_about()
    {
        if($this->session->userdata('logged_in'))
        {
            $data['about'] = $this->Admin_Model->get_about();
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'all-about';
            $this->load->view('home', $data);
        }
    }
    public function all_testimonial()
    {
        if($this->session->userdata('logged_in'))
        {
            $data['testimonials'] = $this->Admin_Model->get_testimonial();
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'all-testimonial';
            $this->load->view('home', $data); 
        }
    }
    public function add_testimonial()
    {
        if($this->session->userdata('logged_in'))
        {
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'add-new-testimonial';
            $this->load->view('home', $data);
        }
    }

    public function contactinfo()
    {
        if ($this->session->userdata('logged_in')) {
            $data['contact'] = $this->Admin_Model->get_contact();
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'contactinfo';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }

    public function add_category()
    {
        if ($this->session->userdata('logged_in')) {
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'add-new-category';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }

    public function add_banner()
    {
        if ($this->session->userdata('logged_in')) {
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'add-banner';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }

    public function all_category()
    {
        if ($this->session->userdata('logged_in')) {
            $data['categories'] = $this->Admin_Model->get_category();
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'categories';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }
    public function sub_category()
    {
        $categories = array();
        $category_id = $this->input->post('id');
        $sub_category = $this->Admin_Model->sub_category($category_id);
        
        $i =0;
        if(count($sub_category->result()) != 0)
        {
            echo json_encode($sub_category->result());
        }
        else
        {
            echo json_encode($categories);
        }
        
    }

    public function all_banner()
    {
        if ($this->session->userdata('logged_in')) {
            $data['banners'] = $this->Admin_Model->get_banner();
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'all-banners';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }

    public function all_recipes()
    {
        if ($this->session->userdata('logged_in')) {
            $data['recipes'] = $this->Admin_Model->get_recipes();
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'all-recipes';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }
    public function all_home()
    {
        if ($this->session->userdata('logged_in')) {
            $data['homes'] = $this->Admin_Model->get_homes();
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'all-home';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }
    public function all_awards()
    {
        if ($this->session->userdata('logged_in')) {
            $data['awards'] = $this->Admin_Model->get_awards();
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'all-awards';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }

    public function all_products()
    {
        if ($this->session->userdata('logged_in')) {
            $keyword = $this->input->post('product-keyword');
            if (empty($keyword)) {
                $data['products'] = $this->Admin_Model->get_products();
                $data['main_view'] = 'admin';
                $data['partial_view'] = 'all-products';
                $this->load->view('home', $data);
            } else {
                $data['products'] = $this->Admin_Model->get_product($keyword);
                $data['main_view'] = 'admin';
                $data['partial_view'] = 'all-products';
                $this->load->view('home', $data);
            }
        } else {
            redirect('home/admin_login');
        }
    }
    public function all_store()
    {
        if ($this->session->userdata('logged_in')) {
            $keyword = $this->input->post('store-keyword');
            if (empty($keyword)) {
                $data['stores'] = $this->Admin_Model->all_store();
                $data['main_view'] = 'admin';
                $data['partial_view'] = 'all-store';
                $this->load->view('home', $data);
            } else {
                $data['stores'] = $this->Admin_Model->store_detail($keyword);
                $data['main_view'] = 'admin';
                $data['partial_view'] = 'all-store';
                $this->load->view('home', $data);
            }
        } else {
            redirect('home/admin_login');
        }
    }
    public function ajax_store()
    {
        $result = $this->Admin_Model->all_store();
        if ($result) {
            echo json_encode($result->result());
        }
        else
        {
            echo "Can't get the information";
        }
    }
    public function add_store()
    {
        if ($this->session->userdata('logged_in')) {
            $data['main_view'] = 'admin';
            $data['partial_view'] = 'add-store';
            $this->load->view('home', $data);
        } else {
            redirect('home/admin_login');
        }
    }
    public function ajax_product()
    {
        $all_products = $this->Admin_Model->ajax_product();
        echo json_encode($all_products->result());
    }
    public function sort_product()
    {
        $sorted_products = $this->Admin_Model->sort_product();
        echo json_encode($sorted_products->result());
    }
    public function product_detail($id = 0)
    {

        if ($id != 0) {
            $results = $this->Admin_Model->product_detail($id);
            $data['product'] = $results['products'];
            $data['recipes'] = $results['recipes'];
            $data['awards'] = $results['awards'];
            $data['main_view'] = 'product-details';
            $this->load->view('home', $data);
        }

    }
    public function recipe_detail($id = 0)
    {
        $result = $this->Admin_Model->recipe_detail($id);
        $data['recipe'] = $result['recipe'];
        if(!empty($result['product']))
        {
            $data['products'] = $result['product'];
        }
        $data['main_view'] = 'recipe-details';
        $this->load->view('home', $data);
    }
    
    public function unset_category()
    {
        $category_id = $this->input->post('id');
        $list_category = $this->session->userdata('category_list');
        if (($key = array_search($category_id, $list_category)) !== false) {
            unset($list_category[$key]);
        }
        $this->session->set_userdata('category_list',$list_category);
        echo json_encode($list_category);
    }
    public function unset_recipe()
    {
        $recipe_id = $this->input->post('id');
        $list_recipe = $this->session->userdata('recipe_list');
        if (($key = array_search($recipe_id, $list_recipe)) !== false) {
            unset($list_recipe[$key]);
        }
        $this->session->set_userdata('recipe_list',$list_recipe);
        echo json_encode($list_recipe);
    }
    public function unset_award()
    {
        $award_id = $this->input->post('id');
        $list_award = $this->session->userdata('award_list');
        if (($key = array_search($award_id, $list_award)) !== false) {
            unset($list_award[$key]);
        }
        $this->session->set_userdata('award_list',$list_award);
        echo json_encode($list_award);
    }
}