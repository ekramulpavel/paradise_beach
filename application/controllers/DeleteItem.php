<?php
/**
 * Created by PhpStorm.
 * User: ashikmahmud
 * Date: 04/09/2017
 * Time: 19:03
 */

class DeleteItem extends CI_Controller
{
    public function delete_product()
    {
        if ($this->session->userdata('logged_in')) {
            $id = $this->input->post('id');
            $result = $this->DeleteItem_Model->delete_product($id);

            if ($result) {
                echo "Product Successfully Deleted";
            }
        } else {
            redirect('home/admin_login');
        }
    }

    public function delete_recipe()
    {
        if ($this->session->userdata('logged_in')) {
            $id = $this->input->post('id');
            $result = $this->DeleteItem_Model->delete_recipe($id);

            if ($result) {
                echo "Recipe Successfully Deleted";
            }
        } else {
            redirect('home/admin_login');
        }
    }
    public function delete_subscriber()
    {
        if($this->session->userdata('logged_in'))
        {
            $id = $this->input->post('id');
            $result = $this->DeleteItem_Model->delete_subscriber($id);

            if ($result) {
                echo "Subscriber Successfully Deleted";
            }
        }
        else
        {
            redirect('admin/all_subscribers');
        }
    }
    public function delete_award()
    {
        if ($this->session->userdata('logged_in')) {
            $id = $this->input->post('id');
            $result = $this->DeleteItem_Model->delete_award($id);

            if ($result) {
                echo "Award Successfully Deleted";
            }
        } else {
            redirect('home/admin_login');
        }
    }
    public function delete_home()
    {
        if ($this->session->userdata('logged_in')) {
            $id = $this->input->post('id');
            $result = $this->DeleteItem_Model->delete_home($id);

            if ($result) {
                echo "Home Successfully Deleted";
            }
        } else {
            redirect('home/admin_login');
        } 
    }

    public function delete_banner()
    {
        if ($this->session->userdata('logged_in')) {
            $id = $this->input->post('id');
            $result = $this->DeleteItem_Model->delete_banner($id);

            if ($result) {
                echo "Banner Successfully Deleted";
            }
        } else {
            redirect('home/admin_login');
        }
    }

    public function delete_category()
    {
        if ($this->session->userdata('logged_in')) {
            $id = $this->input->post('id');
            $result = $this->DeleteItem_Model->delete_category($id);

            if ($result) {
                echo "Category Successfully Deleted";
            }
        } else {
            redirect('home/admin_login');
        }
    }

    public function delete_about()
    {
        if ($this->session->userdata('logged_in')) {
            $id = $this->input->post('id');
            $result = $this->DeleteItem_Model->delete_about($id);

            if ($result) {
                echo "About Section Successfully Deleted";
            }
        } else {
            redirect('home/admin_login');
        }
    }
}