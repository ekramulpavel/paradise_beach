<?php


class Home extends CI_Controller
{
    
    public function index()
    {
        $data['banners'] = $this->Admin_Model->get_banner();
        $data['homes'] = $this->Admin_Model->get_homes();
        $data['testimonials'] = $this->Admin_Model->get_testimonial();
        $data['main_view'] = 'layout/main-layout';
        $this->load->view('home',$data);
    }
    public function admin_login()
    {
        $data['main_view'] ="admin-login";
        $this->load->view('home',$data);
    }
    public function contact()
    {
        $data['contact'] = $this->Admin_Model->get_contact();
        $data['main_view'] ="contact";
        $this->load->view('home',$data);
    }
    public function recipies()
    {
        $data['recipe'] = $this->Admin_Model->get_all_recipe();
        $data['main_view'] ="recipies";
        $this->load->view('home',$data);
    }
    public function products($id = 0)
    {
        if($id !=0)
        {
        $results = $this->Admin_Model->product_detail($id);
        $data['product'] = $results['products'];
        $data['recipes'] = $results['recipes'];
        $data['awards'] = $results['awards'];
        }
        $data['products'] = $this->Admin_Model->get_all_products();
        $data['categories'] = $this->Admin_Model->get_category();
        $data['product_category'] = $this->Admin_Model->get_product_category();
        $data['main_view'] ="products";
        $this->load->view('home',$data);
    }
    public function search_products()
    {
        $keyword = $this->input->post('product-keyword');
        $data['products'] = $this->Admin_Model->search_products($keyword);
        $data['categories'] = $this->Admin_Model->get_category();
        $data['product_category'] = $this->Admin_Model->get_product_category();
        $data['main_view'] = 'products';
        $this->load->view('home', $data);
    }
    public function awards()
    {
        $data['awards'] = $this->Admin_Model->get_awards();
        $data['main_view'] ="awards";
        $this->load->view('home',$data);
    }
    public function aboutus()
    {
        $data['aboutus'] = $this->Admin_Model->get_about();
        $data['main_view'] ="aboutus";
        $this->load->view('home',$data);
    }
    public function where_to_buy()
    {
        $data['main_view'] ="where-to-buy";
        $this->load->view('home',$data);
    }
}