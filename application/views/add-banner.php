<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        ADD BANNER
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/AddItem/add_banner" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <input type="file" style="display: none" name="upload-banner-image" onchange="display_filename(this.id,'display_banner')" id="upload-banner-image" accept="image/*">
                <div class="form-group">
                    <label>Banner Image:</label> <label id="display_banner" style="margin-left: 10px"></label>
                    <button type="button" class="btn btn-primary form-control"
                            onclick="select_file('upload-banner-image')">
                        Upload Image
                    </button>
                </div>
                <div class="form-group">
                    <label>Description:</label>
                    <textarea class="form-control" name="banner-description" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save">
                </div>
            </div>
        </form>
    </div>
</div>