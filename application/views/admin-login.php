<?php $attributes = array('id' => 'login_form', 'class' => "form-horizontal") ?>
<br><br><br><br><br>

<div class="auth-body">
    <div class="container">
        <div class="row">
            <div class="alert alert-danger alert-danger-class" id="alert-danger"></div>
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <?php echo form_open('admin/login', $attributes); ?>
                <div class="panel panel-default" style=" box-shadow: 10px 10px 5px #888888;">
                    <div class="panel-heading text-center" style="font-family: 'museo_slab500';border-bottom:5px solid orangered;">Admin Login</div>
                    <br><br>
                    <div class="panel-body" style="padding: 30px">
                        <div class="form-group">

                            <?php $data = array(
                                'class' => 'form-control',
                                'name' => 'useremail',
                                'placeholder' => 'Your Email',
                                'id' => 'username-field'
                            );
                            ?>
                            <?php echo form_input($data); ?>
                        </div>
                        <br>
                        <div class="form-group">

                            <?php $data = array(
                                'class' => 'form-control',
                                'name' => 'password',
                                'placeholder' => 'Password',
                                'id' => 'password-field'
                            );
                            ?>
                            <?php echo form_password($data); ?>
                        </div>
                        <br>
                        <div class="form-group auth-last-row clearfix">
                            <input type="submit" class="btn btn-primary form-control" value="Login" style="font-family: museo_slab500;font-size: 18px">
                        </div>
                    </div>
                    <br>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</div>
<br><br><br><br><br><br><br><br>