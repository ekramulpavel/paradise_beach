<style type="text/css">
    #gallery{
        background:transparent;
        padding-top: 0px;
    }
</style>
<section id="gallery" class="padding">
    <div class="container">
<h2 class="heading heading_space">Contact<span class="divider-left"></span></h2>
    <div class="col-md-12">
        <div class="text-center">
            <br>
            <h4 class="contact-details">
                <?php foreach($contact->result() as $row){?>
                
                <?php echo $row->address?>
                <br><br>
                <?php echo $row->contactno?>
                <br>
                <?php echo $row->email?>
                
                <?php }?>
            </h4>
        </div>
    </div>
</div>
<br><br>
<div class="container-fluid">
    <div class="col-md-12">
        <div class="">
            <!--<div class="row wow bounceIn" data-wow-delay="300ms">-->
            <div>
                <div id="map"></div>
            </div>
        </div>
    </div>
</div>
</section>
