<!DOCTYPE html>
<html>
<head>
    <title>Paradise Beach</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/edua-icons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/owl.transitions.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/cubeportfolio.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/settings.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/bootsnav.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/loader.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/main.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <style type="text/css">
        .navbar-inverse {
            background-color: #0461b0;
        }
        .hide_product_section{
            display:none;
        }
        body{
            background-color:#fff;
        }
        #navbar-brand {
            transform: translateX(-50%);
            left: 50%;
            position: absolute;
            top: -70px;
        }

        .portfolio-item {
            transition: all .5s;
        }

        .portfolio-wrapper {
            position: relative;
            overflow: hidden;

        }

        .portfolio-wrapper:hover .portfolio-title {
            top: 20%;
        }

        .portfolio-title {
            position: absolute;
            top: -80%;
            background-color: white;
            width: 50%;
            left: 25%;
            opacity: .9;
        }
        .percent-off {
            font-size: 32px;
        }

        .portfolio-item {
            margin-bottom: 5px;
        }
        label{
            color:#00436a;;
        }
        .footer-logo li {
            float: left;
            padding: 30px;
        }

        .footer-parent {
            position: relative;
        }

        .footer-logo {
            position: absolute;
            left: 37%;
        }

        .footer-logo li a {
            display: block;
        }

        .icon-bar {
            border-top: 2px solid green;
        }

        #navbar-header-design {
            position: relative;
        }

        #navbar-header-toggle {
            position: absolute;
            top: -15px;
            left: 10px;
        }
        .productname{
            position: absolute;
            bottom:-30px;
            left:10px;
            color:darkblue;
            z-index:100000;
        }
        .cbp-item-wrapper:hover .productname{
            display: block;
        }

        #navbar-nav-design {
            padding-left: 0;
        }

        #brand-logo {
            width: 170px !important;
        }

        .navbar-nav li a {
            font-size: 18px !important;
        }

        .cbp-filter-item {
            display: block;
            margin: 2px !important;
            padding: 7px 22px !important;
            background-color: #eee !important;
            border-radius: 2px;
            cursor: pointer;
            font-family: 'Raleway', sans-serif !important;
        }

        .cbp-filter-item-active {
            background-color: #03a9f4 !important;
            color: #fff !important;
        }

        .contact-details {
            font-family: GillSans, Arial, Helvetica, sans-serif;
            font-weight: 400;
            line-height: 1.6em;
        }

        .award-design {
            background-color: #fff;

        }

        #award-page {
            background-color: #f0f5f5;
            padding-bottom: 10px;
            margin-bottom: 0 !important;
        }

        .parent-award-design {
            position: relative;
        }

        .award-heading {
            position: absolute;
            left: 30%;
            top: 40%;
        }

        .about-us-design {
            line-height: 1.6em;
            font-family: GillSans, Arial, Helvetica, sans-serif;
        }

        .list-group {
            transition: all .2s;
        }

        .list-group a {
            text-align: center;
        }

        .list-group a:hover {
            background-color: #0461b0;
            font-size: 18px;
            color: #fff;
        }

        .btn-product-details {
            display: none;
        }
        .hide-projects{
            display: none;
        }
        .btn-product-details-show {
            position: absolute;
            bottom: 10px;
            right: 10px;
            z-index: 10000;
        }

        #sidemenu {
            height: 960px;
            background-color: #00436a;
        }

        @media only screen and (max-width: 600px) {
            #footer-navbar-hide {
                display: none;
            }

            #footer-logo {
                left: 15%;
            }
            #rev_slider ul li h1{
                font-size:22px !important;
            }
            #hide-searchbar {
                display: none;
            }

            #top20 {
                margin-top: 50px;
            }

            #navbar-nav-design {
                padding-left: 3%;
            }

            #footer-border {
                border-top: 30px solid #0461b0;
                margin-top: 20px;
            }

            #recipie-mobile {
                margin-top: 20px;
            }

            #mobile-brand-logo {
                position: absolute;
                top: -50px;
                left: 40%;
                width: 100px;
            }

            #sidemenu {
                height: auto;
                background-color: #fff;
            }

            #awards-suggestion, #recipesuggestion {
                width: 100%;
                margin-bottom: 10px;
            }
        }

        /*Side Navbar Design*/
        /********* SIDE NAV BAR ***********/

        li {
            list-style: none;
        }

        .panel-default > .panel-heading {
            color: #fff;
            background-color: #00436a;
            border-color: #ddd;
        }

        .panel-group .panel + .panel {
            margin-top: 0px;
        }

        .panel-group {
            margin-top: 35px;
        }

        .panel-collapse {
            background-color: rgba(220, 213, 172, 0.5);
        }

        .glyphicon {
            margin-right: 10px;
        }

        ul.list-group {
            margin: 0px;
        }

        ul.bulletlist li {
            list-style: disc;
        }

        ul.list-group li a {
            display: block;
            padding: 5px 0px 5px 15px;
            text-decoration: none;
        }

        ul.list-group li {
            border-bottom: 1px dotted rgba(0, 0, 0, 0.2);
        }

        ul.list-group li a:hover, ul li a:focus {
            color: #fff;
            background-color: #00436a;
        }

        .panel-title a:hover,
        .panel-title a:active,
        .panel-title a:focus,
        .panel-title .open a:hover,
        .panel-title .open a:active,
        .panel-title .open a:focus {
            text-decoration: none;
            color: #fff;
        }

        .panel-title > .small, .panel-title > .small > a, .panel-title > a, .panel-title > small, .panel-title > small > a {
            display: block;
        }

        @media (min-width: 768px) {
            #mobile-brand-logo {
                position: absolute;
                top: -50px;
                left: 40%;
                width: 100px;
            }
            #footer-logo {
                left: 27%;
            }
            #rev_slider ul li h1{
                font-size:22px !important;
            }

            #top20 {
                margin-top: 50px;
            }

            #navbar-nav-design {
                padding-left: 0;
            }

            #footer-border {
                border-top: 30px solid #0461b0;
                margin-top: 20px;
            }

            #recipie-mobile {
                margin-top: 20px;
            }
                #footer-border{
                display:none;
            }
        }

        @media (min-width: 992px) {
            .menu-hide {
                display: none;
            }
            #rev_slider ul li h1{
                font-size:40px !important;
            }
            #footer-logo {
                left: 35%;
            }
            #footer-border{
                display:none;
            }

        }
        .menu-hide .panel-default > .panel-heading {
            color: #fff;
            background-color: #8e8c8c;
            border-color: #ddd;
        }

        /********** END SIDEBAR *************/

        /********** NAVBAR TOGGLE *************/

        .navbar-toggle .icon-bar {
            background-color: #fff;
        }

        .navbar-toggle {
            padding: 11px 10px;
            margin-top: 8px;
            margin-right: 15px;
            margin-bottom: 8px;
            background-color: #a32638;
            border-radius: 0px;
        }

        /********** END NAVBAR TOGGLE *************/

        /*Side navbar end*/
    </style>
</head>
<body>
<?php
if (strcmp($main_view, "admin") == 0 || strcmp($main_view, "admin-login") == 0) {
    $this->load->view($main_view);
} else {
    $this->load->view('layout/header');
    $this->load->view($main_view);
   $this->load->view('layout/footer');
}
?>
<!-- Loading google maps with company's location -->
<script>
var map;
var address;
function initMap() 
{
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -33.7839, lng: 151.2672},
        zoom: 14
    });
    var request = {
        location: map.getCenter(),
		radius: '500',
		query: 'Paradise Beach Purveyors'
	};
	var service = new google.maps.places.PlacesService(map);
	service.textSearch(request, callback);
}

function callback(results, status) 
{
	if (status == google.maps.places.PlacesServiceStatus.OK) 
    {
        var placeId = results[0].place_id;
		var service = new google.maps.places.PlacesService(map);
                    
		service.getDetails({placeId: placeId}, function(place, status) 
		{
			if (status === google.maps.places.PlacesServiceStatus.OK) 
			{
                var marker = new google.maps.Marker({map: map, position: place.geometry.location});
                map.setCenter(place.geometry.location);
                place.name = 'Paradise Beach Purveyors';
                var infowindow = new google.maps.InfoWindow();
                google.maps.event.addListener(marker, 'click', function() 
                {
					infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
									   place.formatted_address + '</div>');
					infowindow.open(map, this);
                });
                google.maps.event.addDomListener(window, 'resize', function() {
                    map.setCenter(place.geometry.location);
                });
					infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
									   place.formatted_address + '</div>');
					infowindow.open(map,marker);
			}
		});
	}
	else
	{
	    alert("Status not Ok!");
	}
}
</script>
<script src="<?php echo base_url() ?>assets/js/jquery-2.2.3.js"></script>
<script src="<?php echo base_url() ?>assets/jqueryui/jquery-ui.min.js"></script>

<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBrCc_CBlDyqdWQY8cBhPFzIqhZ_pqSxA&libraries=places&callback=initMap" async defer></script>
   <script src="<?php echo base_url() ?>assets/js/gmaps.min.js"></script> 

<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootsnav.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.appear.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery-countTo.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.parallax-1.1.3.js"></script>
<script src="<?php echo base_url() ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.cubeportfolio.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/revolution.extension.layeranimation.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/revolution.extension.navigation.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/revolution.extension.parallax.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/revolution.extension.slideanims.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/revolution.extension.video.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/functions.js"></script>

<script type="text/javascript">
    $(function () {
        $("#award-date").datepicker();
    });

    function _(el) {
        return document.getElementById(el);
    }
    function no_sub_category()
    {
        _('subcategory').innerHTML ="";
    }
    function show_sub_product(id)
    {
        alert(id)
        _("projects").classList.add('hide_product_section');
    }
    function show_product(id)
    {
        _("sort_tracker").value='atoz';
        $(".cbp-filter-item-active").removeClass("cbp-filter-item-active");
        _(id).classList.add("cbp-filter-item-active");
        if ($(".hide_product_section")[0]){
            // Do something if class exists
            _("set_product").classList.remove('hide_product_section');
            _("product_detail_view").classList.add('hide_product_section');
        }
        var split_id = id.split('-');
        var product_url = "<?php echo base_url()?>index.php/home/products"
        var parent_product =_("set_product");
        parent_product.innerHTML = '';
        var xmlhttp = new XMLHttpRequest();
        var formdata = new FormData();
        formdata.append('id', split_id[1]);
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var products = JSON.parse(xmlhttp.responseText);
                
                if(products.length !=0)
                {
                    for(var i=0;i<products.length;i++)
                    {
                        var product_main_url = product_url+"/"+products[i].id
                        var first_element = document.createElement("div");
                        first_element.setAttribute('style','margin:0;padding:0');
                        first_element.className = 'col-md-3 animated fadeInRight';
                        
                        var card_element = document.createElement("div");
                        card_element.className = 'w3-card-4';
                        first_element.appendChild(card_element);
                        
                        var link = document.createElement("a");
                        link.setAttribute('href',product_main_url);
                        card_element.appendChild(link);
                        
                        
                        var image = document.createElement("img");
                        image.setAttribute('src',products[i].imageurl);
                        image.className = 'imagewidth';
                        image.setAttribute('style','padding-top: 10px;');
                        link.appendChild(image);
                        
                        
                        var productname_container = document.createElement("div");
                        productname_container.className = 'w3-container w3-center';
                        link.appendChild(productname_container);
                        var productname = document.createElement("p");
                        productname.className = 'product-heading';
                        productname.innerHTML = products[i].productname;
                        productname_container.appendChild(productname);
                        
                        parent_product.appendChild(first_element);
                    }
                }
            }
            
        }
        xmlhttp.open("POST", "<?php echo base_url()?>index.php/admin/ajax_product", true);
        xmlhttp.send(formdata);

    }
    function show_subcategory(id)
    {
        var seperate_id = id.split('-');
        var category_id = seperate_id[1];
        _("projects").classList.remove('hide_product_section');
        // location.href = "https://paradise-project-cliverson.c9users.io/index.php/home/products";
        var xmlhttp = new XMLHttpRequest();
        var formdata = new FormData();
        formdata.append('id', category_id);
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var x = JSON.parse(xmlhttp.responseText);
                console.log(x);
                var myNode = _("subcategory");
                myNode.innerHTML = '';
                if(x.length !==0)
                {
                    for(var i=0;i<x.length;i++)
                    {
                        var element = document.createElement("div");
                        element.id = 'sub-'+x[i].id;
                        element.className = 'cbp-filter-item col-md-3 text-center';
                        element.setAttribute('onclick',"show_sub_product(this.id)")
                        element.innerHTML = x[i].categoryname;
                        _('subcategory').appendChild(element);
                        // _('project-filter').innerHTML += "<div class='cbp-filter-item'"+filter_data+">"+x[i]+"</div>";
                    }
                }
                else
                {
                    _('subcategory').innerHTML ="";
                }
            }
        };
        xmlhttp.open("POST", "<?php echo base_url()?>index.php/admin/sub_category", true);
        xmlhttp.send(formdata);
    }
    function sort_product(tracker)
    {
        var formData = new FormData();
        formData.append('sort',tracker);
        var product_url = "<?php echo base_url()?>index.php/home/products"
        var parent_product =_("set_product");
        parent_product.innerHTML = '';
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var products = JSON.parse(xmlhttp.responseText);
                
                if(products.length !=0)
                {
                    for(var i=0;i<products.length;i++)
                    {
                        var product_main_url = product_url+"/"+products[i].id
                        var first_element = document.createElement("div");
                        first_element.setAttribute('style','margin:0;padding:0');
                        first_element.className = 'col-md-3 animated fadeInRight';
                        
                        var card_element = document.createElement("div");
                        card_element.className = 'w3-card-4';
                        first_element.appendChild(card_element);
                        
                        var link = document.createElement("a");
                        link.setAttribute('href',product_main_url);
                        card_element.appendChild(link);
                        
                        
                        var image = document.createElement("img");
                        image.setAttribute('src',products[i].imageurl);
                        image.className = 'imagewidth';
                        image.setAttribute('style','padding-top: 10px;');
                        link.appendChild(image);
                        
                        
                        var productname_container = document.createElement("div");
                        productname_container.className = 'w3-container w3-center';
                        link.appendChild(productname_container);
                        var productname = document.createElement("p");
                        productname.className = 'product-heading';
                        productname.innerHTML = products[i].productname;
                        productname_container.appendChild(productname);
                        
                        parent_product.appendChild(first_element);
                    }
                }
            }
            
        }
        xmlhttp.open("POST", "<?php echo base_url()?>index.php/admin/sort_product", true);
        xmlhttp.send(formData);
    }
    function awards_suggestion() {
        var requesturl = "<?php echo base_url()?>index.php/AddItem/get_awards";
        set_dropdown("Select Awards...", "awards-suggestion", requesturl);
    }

    function recipe_suggestion() {
        var requesturl = "<?php echo base_url()?>index.php/AddItem/get_recipes";
        set_dropdown("Select Recipes...", "recipesuggestion", requesturl);
    }

    function product_category() {
        var requesturl = "<?php echo base_url()?>index.php/AddItem/get_category";
        set_dropdown("Select Category...", "product-category", requesturl);
    }

    function parent_category() {
        var requesturl = "<?php echo base_url()?>index.php/AddItem/get_category";
        set_dropdown("Select Parent Category", "parent-category", requesturl);
    }

    function set_dropdown(text, id, url) {
        var xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var x = JSON.parse(xmlhttp.responseText);
                removeOptions(_(id));
                var selectdefaultoption = document.createElement("option");
                selectdefaultoption.text = text;
                selectdefaultoption.value = 0;
                _(id).appendChild(selectdefaultoption);
                for (var i = 0; i < x.length; i++) {
                    var category = x[i].toString();
                    console.log(category);
                    var categoryname = category.split(",");
                    if (typeof categoryname[1] !== 'undefined') {
                        myOption = document.createElement("option");
                        myOption.text = categoryname[0]+'. '+categoryname[1].substring(0, 20);
                        myOption.value = categoryname[0];
                        _(id).appendChild(myOption);
                    }
                }

            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }
    function delete_product(id) {
        if (delete_confirmation()) {
            var url = "<?php echo base_url()?>index.php/DeleteItem/delete_product";
            delete_item(id, url);
        }
    }

    function delete_recipe(id) {
        if (delete_confirmation()) {
            var url = "<?php echo base_url()?>index.php/DeleteItem/delete_recipe";
            delete_item(id, url);
        }
    }

    function delete_award(id) {
        if (delete_confirmation()) {
            var url = "<?php echo base_url()?>index.php/DeleteItem/delete_award";
            delete_item(id, url);
        }
    }

    function delete_banner(id) {
        if (delete_confirmation()) {
            var url = "<?php echo base_url()?>index.php/DeleteItem/delete_banner";
            delete_item(id, url);
        }
    }

    function delete_category(id) {
        if (delete_confirmation()) {
            var url = "<?php echo base_url()?>index.php/DeleteItem/delete_category";
            delete_item(id, url);
        }
    }

    function delete_about(id) {
        if (delete_confirmation()) {
            var url = "<?php echo base_url()?>index.php/DeleteItem/delete_about";
            delete_item(id, url);
        }
    }
    function delete_home(id)
    {
        if (delete_confirmation()) {
            var url = "<?php echo base_url()?>index.php/DeleteItem/delete_home";
            delete_item(id, url);
        }  
    }
    function delete_subscriber(id)
    {
        if (delete_confirmation()) {
            var url = "<?php echo base_url()?>index.php/DeleteItem/delete_subscriber";
            delete_item(id, url);
        } 
    }
    function delete_confirmation() {
        return confirm("Are you sure you want to delete?")
    }

    function delete_item(id, url) {
        var xmlhttp = new XMLHttpRequest();
        var formdata = new FormData();
        formdata.append('id', id);
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                window.location.reload();
                alert(this.responseText);
            }
        };
        xmlhttp.open("POST", url, true);
        xmlhttp.send(formdata);
    }
    function display_filename(id, displayid) {
        var filename = _(id).files[0].name;
        _(displayid).innerHTML = "("+filename+")";
    }
    function removeOptions(selectbox) {
        var i;
        for (i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }

    function select_file(id) {
        _(id).click();
    }

    function addlist(id, dropdownid, url,name) {
        var selectedvalue = _(dropdownid).value;
        if (selectedvalue !== '0') {
            var suggestion = _(dropdownid);
            var selectedText = suggestion.options[suggestion.selectedIndex].text;


            var xmlhttp = new XMLHttpRequest();
            var formdata = new FormData();
            formdata.append('id', selectedvalue);
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (this.responseText === "success")
                    {
                        var newli = document.createElement("li");
                        if(name.localeCompare('category') == 0)
                        {
                            newli.id = 'category-'+selectedvalue;
                            var btnid ='id=cat-'+selectedvalue;
                            newli.innerHTML = "<div class='row' style='margin-top:3px;'><span style='border-bottom:2px solid darkblue;'><div class='col-md-5'> "+selectedvalue + ". " + selectedText+ "</div><div class='col-md-2'><button type='button' class='btn btn-danger'"+btnid+" onclick='delete_list(this.id)'>Remove</button></div></span></div>";
                            _(id).appendChild(newli);
                        }
                        else if(name.localeCompare('recipe') == 0)
                        {
                            newli.id = 'recipe-'+selectedvalue;
                            var btnid ='id=rec-'+selectedvalue;
                            newli.innerHTML = "<div class='row' style='margin-top:3px;'><span style='border-bottom:2px solid darkblue;'><div class='col-md-5'> "+selectedvalue + ". " + selectedText+ "</div><div class='col-md-2'><button type='button' class='btn btn-danger'"+btnid+" onclick='delete_list(this.id)'>Remove</button></div></span></div>";
                            _(id).appendChild(newli);
                        }
                        else
                        {
                            newli.id = 'award-'+selectedvalue;
                            var btnid ='id=awa-'+selectedvalue;
                            newli.innerHTML = "<div class='row' style='margin-top:3px;'><span style='border-bottom:2px solid darkblue;'><div class='col-md-5'> "+selectedvalue + ". " + selectedText+ "</div><div class='col-md-2'><button type='button' class='btn btn-danger'"+btnid+" onclick='delete_list(this.id)'>Remove</button></div></span></div>";
                            _(id).appendChild(newli);
                        }
                    }
                    else {
                        alert(this.responseText);
                    }
                }
            };
            xmlhttp.open("POST", url, true);
            xmlhttp.send(formdata);
        }
        else {
            alert('Select an item');
        }
    }
    function delete_list(id)
    {
        var url;
        var removeelementid;
        var splitstr = id.split("-");
        if(splitstr[0].localeCompare('cat') == 0)
        {
            url = "<?php echo base_url()?>index.php/Admin/unset_category";
            removeelementid = "category-"+splitstr[1];
        }
        else if(splitstr[0].localeCompare('rec') == 0)
        {
            url = "<?php echo base_url()?>index.php/Admin/unset_recipe";
            removeelementid = "recipe-"+splitstr[1];
        }
        else
        {
            url = "<?php echo base_url()?>index.php/Admin/unset_award";
            removeelementid = "award-"+splitstr[1];
             
        }
        
        var xmlhttp = new XMLHttpRequest();
        var formdata = new FormData();
        formdata.append('id', splitstr[1]);
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                _(removeelementid).remove();    
            }
        };
        xmlhttp.open("POST", url, true);
        xmlhttp.send(formdata);
    }
    function show_details(id) {
        _(id).classList.remove("btn-product-details");
        _(id).classList.add("btn-product-details-show");
    }
    function hide_details(id) {
        _(id).classList.remove("btn-product-details-show");
        _(id).classList.add("btn-product-details");
    }
        function readURL(input,id) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                _(id).setAttribute('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    function Picture_Preview(file,id) {
        readURL(file,id);
    }
    function subscribe()
    {
        var email = _("subscribe_email").value;
        if(validateEmail(email))
        {
            var xmlhttp = new XMLHttpRequest();
            var formdata = new FormData();
            formdata.append('email', email);
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                      _("subscribe_email").setAttribute('style','display:none') ;
                      _("btn_subscribe").setAttribute('style','display:none');
                      _("subscribe_success").innerHTML = this.responseText;
                }
            };
            xmlhttp.open("POST","<?php echo base_url()?>index.php/admin/subscribe_newsletter", true);
            xmlhttp.send(formdata);
        }
        else
        {
            _("subscribe_success").innerHTML = "Enter Valid Email Address";
        }
    }
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
</script>
</body>
</html>