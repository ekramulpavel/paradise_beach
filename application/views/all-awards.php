<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        AWARDS LIST
        </span>
    </div>
</div>
<br>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Award Image</th>
            <th>Description</th>
            <th>Award Date</th>
            <th>Product</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $url = base_url()."index.php/EditItem/edit_award_view";
        foreach ($awards->result() as $row)
        {
            echo "<tr>";
            echo "<td>".$row->id."</td>";
            echo "<td style='width: 25%'><img src='$row->imageurl' width='50%'></td>";
            echo "<td>$row->description</td>";
            echo "<td>$row->awardyear</td>";
            echo "<td>$row->productname</td>";
            echo "<td style='width: 20%'><a class='btn btn-primary' href='$url/$row->id'>Edit</a>
                       <button class='btn btn-danger' onclick='delete_award($row->id)' >Delete</button>
                  </td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
</div>