<style type="text/css">
    .about-us-design{
        font-family: 'Raleway', sans-serif;
        
    }
    p {
        margin: 0000;
    }
    #gallery{
        background:transparent;
    }
</style>
<!--<br><br>-->
<section id="gallery" class="padding">
    <div class="container">
    <h2 class="heading heading_space">About Us<span class="divider-left"></span></h2>
    <br>
    <?php foreach($aboutus->result() as $row) {?>
        <?php if($row->image != null && $row->image!="") {?>
        <div class="col-md-12 top20" style="margin-bottom:20px;">
            <div class = "col-md-6 col-md-offset-3">
                <img src="<?php echo $row->imageurl?>" width="100%"/>
            </div>
        </div>
        <br><?php }?>
    <div class="container" style="">
        <div class="container text-center">
            <p class="about-us-design" style="font-family: 'museo_slab500';">
                <?php echo $row->description?>
                <br><br>
            </p>
        </div>
    </div>
    <?php }?>
</div>
</section>
