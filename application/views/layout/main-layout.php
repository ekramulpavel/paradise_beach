<!--Search-->
<style type="text/css">
.rev_slider_wrapper{
    margin-top:10px !important;
}
#quote-carousel {
    padding: 0 10px 30px 10px;
    margin-top: 60px;
}
#quote-carousel .carousel-control {
    background: none;
    color: #CACACA;
    font-size: 2.3em;
    text-shadow: none;
    margin-top: 30px;
}
#quote-carousel .carousel-indicators {
    position: relative;
    right: 50%;
    top: auto;
    bottom: 0px;
    margin-top: 20px;
    margin-right: -19px;
}
#quote-carousel .carousel-indicators li {
    width: 50px;
    height: 50px;
    cursor: pointer;
    border: 1px solid #ccc;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    border-radius: 50%;
    opacity: 0.4;
    overflow: hidden;
    transition: all .4s ease-in;
    vertical-align: middle;
}
#quote-carousel .carousel-indicators .active {
    width: 128px;
    height: 128px;
    opacity: 1;
    transition: all .2s;
}
.item blockquote {
    border-left: none;
    margin: 0;
}
.item blockquote p:before {
    content: "\f10d";
    font-family: 'Fontawesome';
    float: left;
    margin-right: 10px;
}
.img-hover{
    display:block;
    transition: all;
}
.img-hover:hover{
    background: linear-gradient(to right, rgba(255,255,255,0.5), rgba(255,255,255,.5));
    opacity: 0.5;
}
@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
.subtitle-text{
    position:relative;
    background-color:#00337f;
    color:#fff;
    padding-top: 30px;
    padding-left: 10px;
    padding-bottom: 30px;
    margin-bottom: 10px;
    margin-top: -10px;
}
.banner-subtitle{
    font-size: 1.25em;
    font-weight: 700;
}
.contentsection.justintro {
    padding-top: 0 !important;
}

</style>
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>

<!--Slider-->
<section class="rev_slider_wrapper text-center"> 
  <!-- START REVOLUTION SLIDER 5.0 auto mode -->
  <div id="rev_slider" class="rev_slider"  data-version="5.0">
    <ul>
            <!-- SLIDE  -->
            <?php foreach ($banners->result() as $row) { ?>
                <li data-transition="fade">
                    <!-- MAIN IMAGE -->
                    <img src="<?php echo $row->imageurl?>" alt=""
                         data-bgposition="center center" data-bgfit="cover" data-bgparallax="10" class="rev-slidebg">
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-resizeme"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['326','270','270','150']" data-voffset="['0','0','0','0']"
                         data-responsive_offset="on"
                         data-visibility="['on','on','on','on']"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                         data-start="800">
                        <h1 style="text-shadow:2px 2px 2px #000000;line-height:1.4;position:relative;top:-50px"><?php echo $row->description?></h1>
                    </div>
                </li>
            <?php } ?>
    </ul>
  </div>
  <!-- END REVOLUTION SLIDER --> 
</section>
  <div class="contentsection justintro" >
	<p class="intro" style="font-family: 'Raleway', sans-serif;">There's no stopping us. We use natural,great tasting ingredients in all our flavours. <br>
      We are always experimenting with exciting new flavours and collaborations.
      </p>
   </div>
   <?php $home = $homes->result();?>
   <div class="container-fluid" id="home-banner-section">
       <div class="row">
       <?php foreach($homes->result() as $key => $value) {?>
       <?php if($key ==3) break;?>
           <div class="col-md-4" style="padding-left:5px;padding-right:5px">
               <a href="<?php if(!empty($home[$key]->link)){echo $home[$key]->link;}?>" class="img-hover">
                   <div class="text-center">
                       <img src="<?php echo $home[$key]->imageurl?>" width="100%">
                   </div>
               </a>
               <a href="<?php if(!empty($home[$key]->link)){echo $home[$key]->link;}?>">
                   <div class="subtitle-text">
                        <h4 class="banner-subtitle" style="margin:0;color:#fff"><?php echo $home[$key]->title?></h4>
                        <p style="margin:0;color:#fff"><?php echo $home[$key]->subtitle?>
                        </p>
                   </div>
               </a>
           </div>
           <?php } ?>
         </div>
         <?php if(count($homes->result())>3):?>
        <div class="row">
       <?php for($i=3;$i<count($home);$i++) {?>
           <div class="col-md-4" style="padding-left:5px;padding-right:5px">
               <a href="<?php if(!empty($home[$i]->link)){echo $home[$i]->link;}?>" class="img-hover">
                   <div class="text-center">
                       <img src="<?php echo $home[$i]->imageurl?>" width="100%">
                   </div>
               </a>
               <a href="<?php if(!empty($home[$i]->link)){echo $home[$i]->link;}?>">
                   <div class="subtitle-text">
                        <h4 class="banner-subtitle" style="margin:0;color:#fff"><?php echo $home[$i]->title?></h4>
                        <p style="margin:0;color:#fff"><?php echo $home[$i]->subtitle?>
                        </p>
                   </div>
               </a>
           </div>
           <?php } ?>
         </div>
       <?php endif; ?>
   </div>
<br><br>
<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner text-center">
                        <!-- Quote 1 -->
                        <?php 
                        $i = 0;
                        foreach($testimonials->result() as $row) { ?>
                        <div class="item <?php if($i == 0) echo 'active'; ?>">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <p><?php echo $row->description?></p>
                                        <small><?php echo $row->name?></small>
                                        <?php $i++;?>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <?php }?>

                    <!-- Bottom Carousel Indicators -->
                    <ol class="carousel-indicators">
                        <?php 
                        $i = 0;
                        foreach($testimonials->result() as $row) { ?>
                        <li data-target="#quote-carousel" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0) echo 'active'; ?>"><img class="img-responsive " src="<?php echo $row->imageurl?>" alt="">
                        <?php $i++ ?>
                        </li>
                        <?php } ?>
                    </ol>

                    <!-- Carousel Buttons Next/Prev -->
                    <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i><img src="<?php echo base_url() ?>assets/image/icons/arrow_left.png" height="50px"></i></a>
                    <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i><img src="<?php echo base_url() ?>assets/image/icons/arrow_right.png" height="50px"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>



