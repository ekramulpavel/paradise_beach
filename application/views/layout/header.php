<style type="text/css">
    #navbar-border-bottom-parent{
        position:relative;
        top:-100px;
    }
    #navbar-border-bottom-child{
        position:absolute;
        top:-15px;
        width:100%;
        height:30px;
        border-top:40px solid #0461b0;
        overflow:hidden;
    }
    @media only screen and (min-width: 768px) {
        .rev_slider{
            top: -90px;
        }
    }
    ul li{
        padding:10px;
    }
    .fa-3x{
        font-size: 2.5em;
    }
    a:visited,a:active,a:hover{
        background:transparent;
    }
    nav.navbar.bootsnav ul.nav > li > a {
    color: #0f0f0f;
    font-size: 14px !important;
    font-weight: 500;
    text-transform: uppercase;
    position: relative;
    overflow: hidden;
    font-family: 'Source Sans Pro';
}
.social-icon li a{
    background-color:transparent !important;
}
</style>
<div class="container-fluid" id="hide-searchbar" style="margin-top:20px;">
    <div class="navbar-right" style="display:flex;margin-bottom:20px;">
        <ul style="display:flex;position:relative;top:15px" class="social-icon">
          <li><a target="_blank" href="https://www.facebook.com/ParadiseBeachPurveyorsFoodLoveKitchen/">
              <i>
                  <img src="<?php echo base_url()?>assets/image/icons/facebook-blue.png" width="70%"></img>
              </i></a></li>
          <li>
              <a target="_blank" href="https://www.instagram.com/paradisebeachdips/">
                  <i><img src="<?php echo base_url()?>assets/image/icons/instagram-blue.png" width="70%"></img></i>
              </a>
          </li>
        </ul>
        <form class="form-inline" action="<?php echo base_url()?>index.php/home/search_products" method="post">
            <div class="form-group">
                <input type="text" class="form-control" name="product-keyword" id="email" placeholder="Search" required>
            </div>
            <button type="submit" class="btn btn-primary">Go</button>
        </form>
    </div>
</div>
<nav class="navbar  no-background bootsnav navbar-main" id="top20">
    <div class="container" style="padding-bottom:0px">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header" id="navbar-header-design">
            <a class="navbar-brand visible-xs" href="<?php echo base_url()?>index.php/home">
                <img class="logo" id="mobile-brand-logo"
                     src="<?php echo base_url() ?>assets/image/paradisebeach-log.jpg"
                     alt="Logo" width="10%">
            </a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" id="navbar-header-toggle"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <br>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding-bottom:0;margin-bottom:0">
            <ul class="nav navbar-nav" id="navbar-nav-design" style="padding-left:5%;">
                <li class="
                <?php if(strcmp($_SERVER['REQUEST_URI'],"/index.php/home") == 0)
                {
                    echo "active";
                }
                ?>">
                    <a href="<?php echo base_url()?>index.php/home">Home <span class="sr-only">(current)</span></a>
                </li>
                
                <li class="<?php if(strcmp($_SERVER['REQUEST_URI'],"/index.php/home/products") == 0)
                {
                    echo "active";
                }
                ?>">
                    <a href="<?php echo base_url()?>index.php/home/products">Products</a>
                </li>
                
                <li class="<?php if(strcmp($_SERVER['REQUEST_URI'],"/index.php/home/recipies") == 0)
                {
                    echo "active";
                }
                ?>">
                    <a href="<?php echo base_url()?>index.php/home/recipies">Recipes</a>
                </li>
                <!--<li><a href="events.php">Events</a></li>-->
            </ul>
            <div class="navbar-brand hidden-xs" id="navbar-brand"><a class="white-circle" href="<?php echo base_url()?>index.php/home">
                    <img class="logo" id="brand-logo"
                         src="<?php echo base_url() ?>assets/image/paradisebeach-log.jpg"
                         alt="Logo">
                </a>
            </div>
            <ul class="nav navbar-nav navbar-right" style="padding-right:0">
                
                <li class="<?php if(strcmp($_SERVER['REQUEST_URI'],"/index.php/home/aboutus") == 0)
                {
                    echo "active";
                }
                ?>">
                    <a href="<?php echo base_url()?>index.php/home/aboutus">ABout Us</a>
                </li>
                
                <li class="<?php if(strcmp($_SERVER['REQUEST_URI'],"/index.php/home/awards") == 0)
                {
                    echo "active";
                }
                ?>">
                    <a href="<?php echo base_url()?>index.php/home/where_to_buy">Where To Buy</a>
                </li>
                
                <li class="<?php if(strcmp($_SERVER['REQUEST_URI'],"/index.php/home/contact") == 0)
                {
                    echo "active";
                }
                ?>">
                    <a href="<?php echo base_url()?>index.php/home/contact">Contact</a>
                </li>
                <!--<li><a href="book-now.php">Book Now</a></li>-->
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<div id="navbar-border-bottom-parent">
<div id="navbar-border-bottom-child" style="margin:0"></div>
</div>
