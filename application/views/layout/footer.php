<div id="footer-animation">
  <style type="text/css">
    .footer-parent{
      top:-60px;
    }
    .navbar{
      position:relative;
      top:-100px;
    }
    @media (min-width: 992px){
      #footer-logo {
          left: 40%;
      }
    }
    #footer_row {
      background: #f2f2f2;
      padding:0;
    }
    ul{
      margin:0;
    }
    .footer_menu > ul > li {
      width: 25%;
      float: left;
      margin-bottom: 30px;
      padding: 0;
    }
    ul li{
      padding:0;
    }
    ul li a{
      color:#0461b0;
    }
    @media (min-width: 768px){
      #subscribe_email {
          min-width: 300px !important;
      }
    }
  </style>
</div>
<div id="footer_row">
  <br><br>
        <div class="container">
          <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <nav class="footer_menu">
                        <ul id="menu-footer-menu" class="">
                          <div class="col-md-4">
                            <li id="menu-item-5122" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-5122">
                              <a href="#" style="font-size:1.2em;font-weight:700">Company</a>
                                <ul class="sub-menu">
                                	<li id="menu-item-6146" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6146"><a target="_blank" href="<?php echo base_url() ?>index.php/home/aboutus">About Us</a></li>
                                </ul>
                              </li>
                            </div>
                            <div class="col-md-4">
                              <li id="menu-item-5154" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5154">
                                <a href="#" style="font-size:1.2em;font-weight:700">Our Products</a>
                                  <ul class="sub-menu">
                                  	<li id="menu-item-5186" class="menu-item menu-item-type-taxonomy menu-item-object-recipe_category menu-item-5186"><a href="<?php echo base_url() ?>index.php/home/products">Products</a></li>
                                  	<li id="menu-item-5202" class="menu-item menu-item-type-taxonomy menu-item-object-recipe_category menu-item-5202"><a href="<?php echo base_url() ?>index.php/home/recipies">Recipe</a></li>
                                  	<li id="menu-item-5282" class="menu-item menu-item-type-taxonomy menu-item-object-recipe_category current-recipe-ancestor current-menu-parent current-recipe-parent menu-item-5282"><a href="<?php echo base_url() ?>index.php/home/where_to_buy">Where To Buy</a></li>
                                  </ul>
                                </li>
                              </div>
                              <div class="col-md-4">
                                <li id="menu-item-6066" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-6066">
                                  <a href="#" style="font-size:1.2em;font-weight:700">Connect</a>
                                    <ul class="sub-menu">
                                    	<li id="menu-item-15762" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15762"><a href="<?php echo base_url() ?>index.php/home/contact">Contact Us</a></li>
                                    </ul>
                                  </li>
                                </div>
                      </ul>
                    </nav>
                </div>
              </div>
              <div class="col-md-6">
                <div>
                      <h4 style="font-weight:bold;padding-left:110px;">JOIN OUR NEWSLETTER</h4>
                      <div style="display:flex" >
                        <ul style="display:flex;position:relative;top:15px" class="social-icon">
                        <li><a target="_blank" href="https://www.facebook.com/ParadiseBeachPurveyorsFoodLoveKitchen/">
                            <i>
                                <img src="<?php echo base_url()?>assets/image/icons/facebook-blue.png" width="70%"></img>
                            </i></a></li>
                        <li>
                            <a target="_blank" href="https://www.instagram.com/paradisebeachdips/">
                                <i><img src="<?php echo base_url()?>assets/image/icons/instagram-blue.png" width="70%"></img></i>
                            </a>
                        </li>
                      </ul>
                      <form class="form-inline" id="subscribe" style="margin:0px">
                        <div class="form-group" id="subscribe-group">
                          <input type="email" class="form-control" id="subscribe_email" placeholder="Enter email">
                        </div>
                        <button type="button" class="btn btn-primary" id="btn_subscribe" onclick="subscribe()">JOIN</button>
                        <div id="subscribe_success"></div>
                      </form>
                      </div>
                </div>

              </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                  <br><br>
                    <p id="copyright">© 2018, Paradise Beach</p>
                </div>
            </div>
        </div>
</div>