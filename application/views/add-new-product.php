<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        ADD PRODUCT
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url() ?>index.php/AddItem/add_product" method="post"
              enctype="multipart/form-data">
            <div class="col-md-6">
                <input type="file" style="display: none" name="upload-product-image" id="upload-product-image"
                       accept="image/*" onchange="display_filename(this.id,'display_product')" required>
                <div class="form-group">
                    <label>Product Image:</label><label id="display_product" style="margin-left: 10px"></label>
                    <button type="button" class="btn btn-primary form-control"
                            onclick="select_file('upload-product-image')">
                        Upload Image
                    </button>
                </div>
                <input type="file" name="upload-nutrition-image" id="upload-nutrition-image" accept="image/*"
                       style="display: none" onchange="display_filename(this.id,'display_nutrition')" required>
                <div class="form-group">
                    <label>Nutrition Image:</label><label id="display_nutrition" style="margin-left: 10px"></label>
                    <button type="button" class="btn btn-primary form-control"
                            onclick="select_file('upload-nutrition-image')">
                        Upload Image
                    </button>
                </div>
                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" name="product-name" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Description:</label>
                    <textarea type="text" name="product-description" class="form-control" rows="5" required></textarea>
                </div>
                <div class="form-group">
                    <label>Ingredients:</label>
                    <textarea type="text" name="product-ingredient" class="form-control" rows="5" required></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Allergens</label>
                    <textarea type="text" name="product-allergen" class="form-control" rows="5" required></textarea>
                </div>
                <div class="form-group">
                    <select name="product-category" id="product-category" onmouseover="product_category()"
                            class="form-control">
                        <option selected>Select Category</option>
                    </select>
                    <?php $category_url = base_url() . "index.php/AddItem/set_category";
                    echo "<button type=\"button\" class=\"btn btn-primary\" onclick=\"addlist('category-list','product-category','$category_url','category')\">Add</button>";
                    ?>
                </div>
                <ul id="category-list">

                </ul>
                <div class="form-group">
                    <select name="recipe-suggestion" class="form-control" onmouseover="recipe_suggestion()"
                            id="recipesuggestion">
                        <option value="0" selected>Recipe Suggestion</option>
                    </select>
                    <?php
                    $recipe_url = base_url() . "index.php/AddItem/set_recipes";
                    echo "<button type=\"button\" class=\"btn btn-primary\" onclick=\"addlist('recipe-list','recipesuggestion','$recipe_url','recipe')\">Add</button>";
                    ?>
                </div>
                <ul id="recipe-list">

                </ul>
                <div class="form-group">
                    <select name="awards-suggestion" class="form-control" onmouseover="awards_suggestion()"
                            id="awards-suggestion">
                        <option value="0" selected>Add Awards</option>
                    </select>
                    <?php $awards_url = base_url() . "index.php/AddItem/set_awards";
                    echo "<button type=\"button\" class=\"btn btn-primary\" onclick=\"addlist('award-list','awards-suggestion','$awards_url','award')\">Add</button>"
                    ?>
                </div>
                <ul id="award-list">

                </ul>
                <br>
                <div class="form-group" style="float:right">
                    <input type="submit" class="btn btn-primary" value="Save Product">
                </div>
            </div>
        </form>
    </div>
</div>