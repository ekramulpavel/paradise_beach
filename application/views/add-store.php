<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        ADD STORE
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/AddItem/add_store" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" name="store-name" class="form-control">
                </div>
                <div class="form-group">
                    <label>Address:</label>
                    <input type="text" name="store-address" class="form-control">
                </div>
                <div class="form-group">
                    <label>District:</label>
                    <input type="text" name="store-district" class="form-control">
                </div>
                <div class="form-group">
                    <label>State:</label>
                    <input type="text" name="store-state" class="form-control">
                </div>
                <div class="form-group">
                    <label>Post Code:</label>
                    <input type="text" name="store-postcode" class="form-control">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save">
                </div>
            </div>
        </form>
    </div>
</div>