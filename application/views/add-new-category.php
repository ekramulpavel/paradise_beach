<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        ADD CATEGORY
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/AddItem/add_category" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" name="category-name" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Parent Category:</label>
                    <select name="parent-category" class="form-control" onmouseover="parent_category()" id="parent-category">
                        <option value="0" selected>Select Parent Category</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save">
                </div>
            </div>
        </form>
        <div id="addIO"></div>
    </div>
</div>