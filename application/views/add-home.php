<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        ADD HOME
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/AddItem/add_home" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <input type="file" style="display: none" name="upload-home-image" id="upload-home-image" onchange="display_filename(this.id,'display_home')" accept="image/*" required>
                <div class="form-group">
                    <label>Home Image:</label><label id="display_home" style="margin-left: 10px"></label>
                    <button type="button" class="btn btn-primary form-control"
                            onclick="select_file('upload-home-image')">
                        Upload Image
                    </button>
                </div>
                <div class="form-group">
                    <label>Title:</label>
                    <input type="text" name="home-title" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Subtitle:</label>
                    <input class="form-control" name="home-subtitle" required>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save">
                </div>
            </div>
        </form>
    </div>
</div>