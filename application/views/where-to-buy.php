<style type="text/css">
    label {
    float: none; /* if you had floats before? otherwise inline-block will behave differently */
    display: inline-block;
    vertical-align: middle;
}
select{
    width: auto;
    display: inline-block;
    background:#fff;
    border:1px solid black;
    border-radius:5px;
}
input{
    border-radius:5px;
}
input[type="submit"]
{
    height:49px !important;
}
#store-list li{
    padding:10px;
    line-height:25px;
    font-weight:200;
    border-bottom:1px dotted #ccc;
}
#loc-form-padding label{
    color: #fff;
}
</style>

<section id="where-to-buy" class="padding">
    <div class="container">
            <h2 style="margin:0 0 10px;">Find your local stockist of Paradise Beach</h2>
            <p>Simply enter your postcode below and the map will&nbsp;provide you with the closest stores to your location.</p>
        <!--Search Bar section-->
        <div id="loc-form-padding" style="background-color:#0461b0;margin-bottom:10px;">
          <form id="location-form" action="#" style="padding: 13px 0 13px 0">
              <label for="your-location" style="margin-left:20px">Your location</label>
              <input type="text" name="" id="your-location"/>
              
              <label for="select-distance">Search radius</label>
              <select id="select-distance">
                  <option value="5">5 km</option>
                  <option value="10">10 km</option>
                  <option value="25">25 km</option>
                  <option value="100" selected>100 km</option>
                  <option value="200">200 km</option>
              </select>
              
              <label for="result">Results</label>
              <select id="result">
                  <option value="10" selected>10</option>
                  <option value="25">25</option>
                  <option value="50">50</option>
                  <option value="100">100</option>
              </select>
              <button type="button" class="btn btn-default" onClick="findDistance()">Search</button>
          </form>
          </div>
        <!--End Search Bar section-->
        
        <!--Row which will hold two column one for search result and another for Google Map-->
        <div class="row">
            <div class="col-md-4" style="height:500px;">
                <div id="sotres">
                    <ul id="store-list"></ul>
                </div>
            </div>
            <div class="col-md-8">
                <div id="mapArea" style="width:100%;height:500px"></div>
            </div>
        </div>
    </div>
</section>
<!--Google Maps Code Will Go Inside This Script-->
<script type="text/javascript">


//First Get Latitude and Longitude from address
//Calculate distance between two points.
// If matches the location inside the given distance then add marker to the google maps.
var map,latlngbounds;
function findDistance()
{
    if (navigator.geolocation){
        navigator.geolocation.getCurrentPosition(onSuccess, onError, {maximumAge:60*1000, timeout:5*60*1000, enableHighAccuracy:true});
    }
    var input = document.getElementById("your-location");
    var autocomplete = new google.maps.places.Autocomplete(input);
          google.maps.event.addListener(autocomplete, 'place_changed', function(){
             var place = autocomplete.getPlace();
         });
}
function onSuccess(position){
    var latSource = position.coords.latitude;
    var longSource = position.coords.longitude;
    
    //get the address from the textbox
    var addr = document.getElementById("your-location").value;
    //get the distance from the dropbox
    var distanceDropdown = document.getElementById("select-distance");
    var expectedDistance = distanceDropdown.options[distanceDropdown.selectedIndex].value;

    if(addr !="")
    {
        //change the source address
        latlongsource = getLatLong(addr);
        latSource = latlongsource.latitude;
        longSource = latlongsource.longitude;
    }
    
    //get all the locations from the database.
    var xmlhttpAddr = new XMLHttpRequest();
    var url = "<?php echo base_url() ?>index.php/Admin/ajax_store";
    var storeList = document.getElementById('store-list');
    var finalStore = [];
    storeList.innerHTML = "";
    xmlhttpAddr.onreadystatechange = function () {
        if(xmlhttpAddr.readyState == 4 && xmlhttpAddr.status == 200)
        {
            var result = xmlhttpAddr.responseText;
            var stores = JSON.parse(result);
            console.log(stores);
            var store_list = "";
            for(var i=0;i<stores.length;i++)
            {
                //check the destination from the source.
                //if the distance value is less than dropbox selected value then we will add the value in another array
                    var latlong = getLatLong(stores[i].address);
                    var longDest = latlong.longitude;
                    var latDest = latlong.latitude;
                    var dist = Math.round(calculateDist(latSource, longSource, latDest, longDest)*100)/100;
                    if(dist < expectedDistance)
                    {
                        var resul_object = {latitude:latDest,longitude:longDest,name:stores[i].name,address:stores[i].address,district:stores[i].district,state:stores[i].state};
                        //split the address
                        var address = stores[i].address;
                        var splitaddress = address.split(",");
                        console.log(splitaddress);
                        var newaddress = "";
                        for(var j=0;j<splitaddress.length;j++)
                        {
                            newaddress +=splitaddress[j]+"<br>";
                        }
                        store_list = "<li>" +"<strong>"+stores[i].name+"</strong><br>"+newaddress+""+dist+" km "+"</li>";
                        storeList.innerHTML += store_list;
                        finalStore.push(resul_object);
                    }
            }
            if(finalStore.length > 0)
            {
                console.log(finalStore);
                drawMap(finalStore);
            }
            else
            {
                //show the map with your current location
                var resul_object = {latitude:latSource,longitude:longSource,name:"",address:"",district:"",state:""};
                finalStore.push(resul_object);
                drawMap(finalStore);
                storeList.innerHTML = "No result found ....";
                console.log("No result found...");
            }
        }
    }
    xmlhttpAddr.open("GET",url,true);
    xmlhttpAddr.send();

}
function onError(error)
{
    
}
function drawMap(stores)
{
    console.log(stores);
    var latitude = stores[0].latitude;
    var longitude = stores[0].longitude;
    var centerLocation = new google.maps.LatLng(latitude,longitude);
    latlngbounds = new google.maps.LatLngBounds();
    var mapOptions = {
        center:centerLocation,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("mapArea"),mapOptions);
    for(var i=0;i<stores.length;i++)
    {
        drawMarker(stores[i]);
    }
}
function drawMarker(store)
{
    console.log(store);
    var location = new google.maps.LatLng(store.latitude,store.longitude);
    marker = new google.maps.Marker({
        position:location,
        map:map
    })
    latlngbounds.extend(marker.position);
    var infoWindow = new google.maps.InfoWindow({
        content: "Store: "+ store.name + "<br>"+"Address: "+store.address+"<br>"+"District: "+store.district
    });
    google.maps.event.addListener(marker,'click',function(){
        infoWindow.open(map,this)
    });
}
function getLatLong(address)
{
    var localAddress = address.replace(" ","+")
    console.log(address);
    var xmlhttpAddr = new XMLHttpRequest();
    var url = "https://maps.googleapis.com/maps/api/geocode/json?address="+localAddress;
    xmlhttpAddr.open("GET",url,false);
    xmlhttpAddr.send();
    if(xmlhttpAddr.readyState == 4 && xmlhttpAddr.status == 200)
    {
        var result = xmlhttpAddr.responseText;
        var jsonResult = eval("("+result+")");
        var lat = jsonResult.results[0].geometry.location.lat;
        var long = jsonResult.results[0].geometry.location.lng;
        return {latitude:lat,longitude:long}
    }
}
function calculateDist(latSource, longSource, latDest, longDest) {
   var latSourceRadians = latSource*Math.PI/180;
   var longSourceRadians = longSource*Math.PI/180; 
   var latDestRadians = latDest*Math.PI/180;
   var longDestRadians = longDest*Math.PI/180;
   var distance = 3959 * Math.acos(
       Math.cos(latSourceRadians) * Math.cos(latDestRadians) *
       Math.cos(longSourceRadians - longDestRadians) +
       Math.sin(latSourceRadians) * Math.sin(latDestRadians)
       );
      distance = distance * 1.609344; //Distance in Km 
      return distance;
}

    if (window.addEventListener) {
        window.addEventListener('load', findDistance, false); //W3C
    }
    else {
        window.attachEvent('onload', findDistance); //IE
    }
</script>

