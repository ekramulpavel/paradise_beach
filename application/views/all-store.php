<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        STORE LIST
        </span>
    </div>
</div>
<br>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Address</th>
            <th>District</th>
            <th>State</th>
            <th>Post Code</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $url = base_url()."index.php/EditItem/edit_store_view";
        foreach ($stores->result() as $row)
        {
            echo "<tr>";
            echo "<td>".$row->id."</td>";
            echo "<td style='width: 20%'>$row->name</td>";
            echo "<td style='width: 20%'>$row->address</td>";
            echo "<td style='width: 20%'>$row->district</td>";
            echo "<td style='width: 20%'>$row->state</td>";
            echo "<td style='width: 20%'>$row->post_code</td>";
            echo "<td style='width: 20%'><a class='btn btn-primary' href='$url/$row->id'>Edit</a>
                       <button class='btn btn-danger' onclick='delete_store($row->id)' >Delete</button>
                  </td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
</div>