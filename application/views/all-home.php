<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        HOMES LIST
        </span>
    </div>
</div>
<br>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Home Image</th>
            <th>Title</th>
            <th>Subtitle</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $url = base_url()."index.php/EditItem/edit_home_view";
        foreach ($homes->result() as $row)
        {
            echo "<tr>";
            echo "<td>".$row->id."</td>";
            echo "<td style='width: 20%'><img src='$row->imageurl' width='80%'></td>";
            echo "<td style='width: 20%'>$row->title</td>";
            echo "<td style='width: 30%'>$row->subtitle</td>";
            echo "<td style='width: 20%'><a class='btn btn-primary' href='$url/$row->id'>Edit</a>
                       <button class='btn btn-danger' onclick='delete_home($row->id)' >Delete</button>
                  </td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
</div>