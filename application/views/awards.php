<style type="text/css">
    #award-page{
            font-family: 'Raleway', sans-serif;
            background-color: #fff;
    }
    #gallery
    {
        background-color: #FFFFF0;
    }
</style>
<section id="gallery" class="padding">
    <div class="container">
    <h2 class="heading">Awards<span class="divider-left"></span></h2>
    
    <?php $firstdate = $awards->result();
    $firstyear = date('Y', strtotime($firstdate[0]->awardyear));
    $i = 0;
    ?>
    <div class="container text-center" >
        <?php foreach($awards->result() as $row) {?>
        <?php $awardyear = $row->awardyear;
        $updated_year = date('Y', strtotime($row->awardyear));
        ?>
        <?php if($i == 0){
        echo "<h2>$updated_year</h2>";
        $i++;
        }?>
        <?php if(strcmp($firstyear,$updated_year) != 0 && $i != 0){
            echo "<h2>$updated_year</h2>";
            $firstyear = $updated_year;
            
        }?><div class="container" style='margin-bottom:30px;'>
            <div class="row " >
                <div class="col-md-8 col-md-offset-2 award-design" style="border: 3px solid #00436a;">
                    <div class="col-md-4 ">
                        <img src="<?php echo $row->imageurl?>" class="img-responsive img-circle" width="50%"/>
                    </div>
                    <div class="col-md-8">
                        <h4 class="text-justify award-heading-des" style="padding-top:20px;padding-bottom:15px;font-family: 'museo_slab500';line-height:1.6em;"><?php echo $row->description;?></h4>
                    </div>
                </div>
            </div>
            </div>
        <?php } ?>
    </div>
    </div>
</div>
</section>