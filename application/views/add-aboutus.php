<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        ADD ABOUT US SECTION
        </span>
        <br><br>
        <form action="<?php echo base_url()?>index.php/AddItem/add_aboutus" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <input type="file" style="display: none" name="upload-about-image" id="upload-about-image" onchange="display_filename(this.id,'display_aboutus')" accept="image/*">
                <div class="form-group">
                    <label>Image:</label><label id="display_aboutus" style="margin-left: 10px"></label>
                    <button type="button" class="btn btn-primary form-control"
                            onclick="select_file('upload-about-image')">
                        Upload Image
                    </button>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea name="about-description" class="form-control" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save">
                </div>
            </div>
        </form>
    </div>
</div>