<style type="text/css">
    #gallery{
        background:transparent;
    }
    h3{
        font-weight: bold !important;
    }
</style>
<section id="gallery" class="padding">
    <div class="container">
    <h2 class="heading heading_space">Recipe Detail<span class="divider-left"></span></h2>
    <?php foreach($recipe->result() as $row){?>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <img src="<?php echo $row->imageurl?>" width="100%">
                </div>
                <div class="col-md-9" id="recipie-mobile">
                    
                    
                    <div class="first-section" >
                        <h3>Recipe Name:</h3>
                        <p style="font-family: 'museo_slab500';line-height:1.6em">
                            <?php echo $row->title?>
                        </p>
                    </div>
                    <div class="first-section" >
                        <h3>Ingredients</h3>
                        <p style="font-family: 'museo_slab500';line-height:1.6em">
                            <?php echo $row->ingredient?>
                        </p>
                    </div>
                    <div class="first-section">
                       <h3>Recipe Procedure</h3> 
                       <p style="font-family: 'museo_slab500';line-height:1.6em">
                           <?php echo $row->recipe_procedure; ?>
                       </p>
                    </div>
                    <?php if(!empty($products)): ?>
                    <div class="first-section">
                        <h3>Products</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="list-group">
                                    <?php $product_detail_url = base_url()."index.php/home/products"?>
                                    <?php foreach($products->result() as $row) {?>
                                  <a href="<?php echo $product_detail_url.'/'.$row->id?>" class="list-group-item list-group-item-action">
                                      <?php echo $row->productname?>
                                  </a>
                                  <?php } ?>
                                  </div>
                              </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php } ?>
    </div>
</section>
