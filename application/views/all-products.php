<style type="text/css">
    #searchbox {
        float:right;
        clear:both;
    }
</style>
<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        PRODUCTS LIST
        </span>
    </div>
</div>
<input type="text" value="atoz" id="admin_sort_tracker" style="display:none">
<form action="<?php echo base_url()?>index.php/admin/all_products" method="post">
    <div class="form-inline" id="searchbox">
        <div class="form-group">
        <div class="cbp-filter-item" style="height:45px;width:130px;position:relative" onclick="sort_admin_product()">
        <img src="<?php echo base_url()?>assets/image/sort.png"  width="40%" style="position:absolute;top:0;left:40px">
    </div>
        </div>
        <div class="form-group">
            <input type="text" name="product-keyword" class="form-control" placeholder="serach by name or description">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="search">
        </div>
    </div>
</form>
<br>
<div class="table-responsive" style="clear:both">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Product Image</th>
            <th>Product Name</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody id="products_body">
        <?php
        $url = base_url()."index.php/EditItem/edit_product_view";
        foreach ($products->result() as $row) {
            $stringpart =substr($row->description,0,50); 
            echo "<tr>";
            echo "<td>" . $row->id . "</td>";
            echo "<td style='width: 20%'><img src='$row->imageurl' width='80%'></td>";
            echo "<td style='width: 20%'>$row->productname</td>";
            echo "<td style='width: 30%'>
            $stringpart....
            </td>";
            echo "<td style='width: 20%'><a class='btn btn-primary' href='$url/$row->id'>Edit</a>
                       <button class='btn btn-danger' onclick='delete_product($row->id)' >Delete</button>
                  </td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
</div>


<script type="text/javascript">
    function sort_admin_product()
    {
        var formData = new FormData();
        
        var sort_tacker = document.getElementById("admin_sort_tracker");
        if(sort_tacker.value == 'atoz')
        {
            formData.append('sort','atoz');
            sort_tacker.value = 'ztoa';
            
        }
        else
        {
            formData.append('sort','ztoa');
            sort_tacker.value = 'atoz';
        }
        var tbody = document.getElementById("products_body");
        tbody.innerHTML = '';
        var xmlhttp = new XMLHttpRequest();
        var editItemUrl = "https://www.paradisebeach.ws/test/index.php/EditItem/edit_product_view";
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var products = JSON.parse(xmlhttp.responseText);

                for(var i=0;i<products.length;i++)
                {
                    var editurl = editItemUrl+"/"+products[i].id;
                    var deleteProduct = "delete_product("+products[i].id+")";
                    var action = "<td style='width: 20%'>" + 
                        "<a class='btn btn-primary' href="+editurl+">Edit</a>"+
                        "<button class='btn btn-danger' onclick="+deleteProduct+">Delete</button>"+
                    "</td>";
                    var imageurl = ""+products[i].imageurl;
                    var row = "<tr>"+"<td>"+products[i].id+"</td>"+"<td style='width: 20%'><img width='80%' src="+imageurl+"></td>"+"<td style='width: 20%'>"+products[i].productname+"</td>"+"<td style='width: 30%'>"+products[i].description+"</td>"+action+"</tr>";
                    tbody.innerHTML += row;
                    
                    console.log(products[i]);
                }
            }

        };
        xmlhttp.open("POST", "<?php echo base_url()?>index.php/admin/sort_product", true);
        xmlhttp.send(formData);
    }
</script>