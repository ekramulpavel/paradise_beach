<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        ADD RECIPE
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/AddItem/add_recipe" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <input type="file" style="display: none" name="upload-recipe-image" id="upload-recipe-image" onchange="display_filename(this.id,'display_recipe')" accept="image/*">
                <div class="form-group">
                    <label>Image:</label><label id="display_recipe" style="margin-left: 10px"></label>
                    <button type="button" class="btn btn-primary form-control"
                            onclick="select_file('upload-recipe-image')">
                        Upload Image
                    </button>
                </div>
                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" name="recipe-title" class="form-control">
                </div>
                <div class="form-group">
                    <label>Ingredients:</label>
                    <textarea type="text" name="recipe-ingredient" class="form-control" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <label>Procedures:</label>
                    <textarea type="text" name="recipe-procedure" class="form-control" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save">
                </div>
            </div>
        </form>
    </div>
</div>