<style type="text/css">
.img,.w3-card-4 {
    margin: 5px;
    padding: 0;
    border: 0;
    outline: 0;
    font-size: 100%;
    vertical-align: baseline;
    background: transparent;
}
#gallery{
    background:transparent;
    padding-top: 0px;
}
.imagewidth{
    width:90%;
}
.w3-card-4{
    background-color:#fff;
    transition:all 2s ease;
}
.product-heading{
    color: #fff;
    background-color: #00337f;
    padding: 10px 10px;
    margin:0px;
    width:93%;
    position:relative;
    top:-15px;
}
</style>
<!-- Products -->
<section id="gallery" class="padding">
    <div class="container">
            <div class="col-sm-5">
                <h2 class="heading heading_space">Products<span class="divider-left"></span></h2>
            </div>
        <div class="row">
            <input type="text" value="atoz" id="sort_tracker" style="display:none">
            <div class="col-sm-12 text-center">
                <div id="project-filter" class="cbp-l-filters-alignRight">
                    <div class="cbp-filter-item" onclick="sort_product('atoz')">A to Z
                    </div>
                    <div class="cbp-filter-item" onclick="sort_product('ztoa')">Z to A
                    </div>
                    <div data-filter="*" class="cbp-filter-item" id="cid-0" onclick="show_product(this.id)">All Products</div>
                    <?php foreach ($categories->result() as $row) {?>
                    <?php $trim_str = str_replace(' ', '', $row->categoryname);?>
                    <?php if (empty($row->categoryid)): ?>
                    <div data-filter=".<?php echo $trim_str?>" class="cbp-filter-item" id="<?php echo 'cid-'.$row->id?>" onclick="show_product(this.id)"><?php echo $row->categoryname?></div>
                    <?php endif; ?>
                    <?php }?>
                </div>
            </div>
        </div>
        <div class="row <?php if(!empty($product)){echo 'hide_product_section';}?>" id="set_product">
            <?php $product_details_url = base_url()."index.php/home/products"?>
            <?php foreach ($products->result() as $row) { ?>
            <div class="col-md-3 animated fadeInRight" style="margin:0;padding:0;">
                <div class="w3-card-4">
                    <a href="<?php echo $product_details_url.'/'.$row->id?>">
                      <img  src="<?php echo $row->imageurl?>" width="90%" alt="Norway" class="img" style="padding-top: 10px;">
                      <div class="w3-container w3-center">
                        <p class="product-heading"><?php echo $row->productname?></p>
                      </div>
                  </a>
                </div>
            </div>
            <?php }?>
        </div>
        <?php if(!empty($product)):?>
        <?php $this->load->view('product-details')?>
        <?php endif; ?>
    </div>
</section>
<!-- Products -->