<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;">
        CONTACT INFO
        </span>
        <form action="<?php echo base_url()?>index.php/EditItem/edit_contact" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <br>
                <?php foreach($contact->result() as $row) { ?>
                <input type="text" name="contact_id" value="<?php echo $row->id?>" style="display:none;">
                <div class="form-group">
                    <label>Contact No:</label>
                    <input type="text" class="form-control" name="contactno" value="<?php echo $row->contactno?>">
                </div>
                <div class="form-group">
                    <label>Address:</label>
                    <textarea type="text" class="form-control" name="address" rows="5"><?php echo $row->address?></textarea>
                </div>
                <div class="form-group">
                    <label>Email:</label>
                    <input type="text" class="form-control" name="email" value="<?php echo $row->email?>">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Update">
                </div>
                <?php } ?>
            </div>
        </form>
    </div>
</div>