<style type="text/css">
    h3{
        font-weight: bold !important;
    }
</style>

<div class="container-fluid" id="product_detail_view">
    <div class="row">
        <div class="col-md-4">
            <!--Product Image-->
            <div class="text-center">
            <?php foreach ($product->result() as $row) { ?>
                <img src="<?php echo $row->imageurl ?>" width="80%">
            <?php } ?>
            </div>
            <br><br>
                <!--Nutrition Image-->
                <div class="text-center">
                    <?php foreach ($product->result() as $row){ ?>
                        <h3>Nutrition Table:</h3>
                        <img src="<?php echo $row->nutritionfileurl?>" alt="" width="70%">
                    <?php }?>
                </div>
            <?php if(!empty($awards)): ?>
            <br>
            <h3 class="text-center">Awards</h3>
            <?php foreach ($awards->result() as $row) { ?>
            <div class="row">
                <div class="col-md-3">
                <img src="<?php echo $row->imageurl ?>" width="100%">
                </div>
                <div class="col-md-9 text-justify" style="font-family: 'museo_slab500';line-height:1.6em;font-size:14px;">
                <?php echo $row->description?>
                </div>
            </div>
            <?php } ?>
            <?php endif; ?>
        </div>
        <div class="col-md-8">
            <?php foreach ($product->result() as $row){ ?>
            <h3><?php echo $row->productname ?></h3>
            <br>
            <h4 style="font-family: 'museo_slab500';line-height:1.6em"><?php echo $row->description ?></h4>
            <!--Name of the product
            product description-->
            <div class="row">
                <br>
                <div class="col-md-12">
                    <h3>Ingredients</h3>
                    <br>
                    <p style="font-family: 'museo_slab500';line-height:1.6em;font-size:14px;" class="text-justify"><?php echo $row->ingredients ?></p>
                    <h3>Allergens</h3>
                    <br>
                    <p style="font-family: 'museo_slab500';line-height:1.6em;font-size:14px;" class="text-justify"><?php echo $row->allergens ?> </p>
                    <?php } ?>
                    <!--ingredient allergen recipe suggestion -->
                    <div class="row">
                        <div class="col-md-6">
                    <?php if(!empty($recipes)): ?>
                    <h3>Recipes</h3>
                    <div class="list-group">
                        <?php $recipe_detail_url = base_url()."index.php/admin/recipe_detail"?>
                        <?php foreach ($recipes->result() as $row) { ?>
                          <a href="<?php echo $recipe_detail_url.'/'.$row->id?>" class="list-group-item list-group-item-action"><?php echo $row->title?></a>
                        <?php } ?>
                    </div>
                    <?php endif; ?>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>