<style type="text/css">
img,.w3-card-4 {
    margin: 0;
    padding: 0;
    border: 0;
    outline: 0;
    font-size: 100%;
    vertical-align: baseline;
    background: transparent;
}
#gallery{
    background:transparent;
    padding-top: 0px;
}
.w3-card-4{
    background-color:#fff;
    transition:all 2s ease;
}
.product-heading{
    color: #fff;
    background-color: #00337f;
    padding: 10px 10px;
    margin:0;
    height:60px;
    width:90%;
    position:relative;
    top:-10px;
}
</style>
<section id="gallery">
    <div class="container">
    <h2 class="heading heading_space">Recipes<span class="divider-left"></span></h2>
    <br>
            <div class="row" id="set_product">
            <?php $recipe_detail_url = base_url()."index.php/admin/recipe_detail"?>
            <?php foreach ($recipe->result() as $row) { ?>
            <div class="col-md-4 animated fadeInRight" style="margin:0;padding:0;">
                <div class="w3-card-4">
                    <a href="<?php echo $recipe_detail_url.'/'.$row->id?>">

                      <img  src="<?php echo $row->imageurl?>" width="90%" alt="Norway" style="padding-top: 10px;">
                      <div class="w3-container w3-center">
                        <p class="product-heading"><?php echo $row->title?></p>
                  </div>
                  </a>
                </div>
            </div>
            <?php }?>
        </div>
</div>
</section>
