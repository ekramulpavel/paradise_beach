<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        ADD TESTIMONIAL
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/AddItem/add_testimonial" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <input type="file" style="display: none" name="upload-testimonial-image" id="upload-testimonial-image" onchange="display_filename(this.id,'display_testimonial')" accept="image/*">
                <div class="form-group">
                    <label>Image (128 &times; 128): </label><label id="display_testimonial" style="margin-left: 10px"></label>
                    <button type="button" class="btn btn-primary form-control"
                            onclick="select_file('upload-testimonial-image')">
                        Upload Image
                    </button>
                </div>
                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label>Description:</label>
                    <textarea class="form-control" name="description" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save Testimonial">
                </div>
            </div>
        </form>
    </div>
</div>