<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        EDIT RECIPE
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/EditItem/edit_recipe" method="post" enctype="multipart/form-data">
            <?php foreach ($recipes->result() as $row){?>
                <input type="text" value="<?php echo $row->id?>" name="recipe_id" style="display: none">
            <div class="col-md-6">
                <div class="text-center">
                    <img src="<?php echo $row->imageurl;?>" width="30%" id="set_recipe_image">
                </div>
                <br>
                <input type="file" name="recipe-image" id="recipe-image"
                       onchange="Picture_Preview(this,'set_recipe_image')" style="display: none">
                <div class="form-group">
                    <button type="button" class="btn btn-primary form-control"
                            onclick="select_file('recipe-image')">Change Recipe Image
                    </button>
                </div>
                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" name="recipe-title" value="<?php echo $row->title?>" class="form-control">
                </div>
                <div class="form-group">
                    <label>Ingredients:</label>
                    <textarea type="text" name="recipe-ingredient" class="form-control" rows="5"><?php echo $row->ingredient?></textarea>
                </div>
                <div class="form-group">
                    <label>Procedures:</label>
                    <textarea type="text" name="recipe-procedure" class="form-control" rows="5"><?php echo $row->recipe_procedure?></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Update Recipe">
                </div>
            </div>
            <?php }?>
        </form>
    </div>
</div>