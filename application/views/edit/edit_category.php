<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        EDIT CATEGORY
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/EditItem/edit_category" method="post" enctype="multipart/form-data">
            <?php foreach ($categories->result() as $row){?>
                <input type="text" value="<?php echo $row->id?>" name="category_id" style="display: none">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" name="category-name" value="<?php echo $row->categoryname?>" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Parent Category:</label>
                    <select name="parent-category" class="form-control" onmouseover="parent_category()" id="parent-category">
                        <option value="0" selected>Select Parent Category</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Update">
                </div>
            </div>
            <?php }?>
        </form>
        <div id="addIO"></div>
    </div>
</div>