<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        EDIT PRODUCT
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url() ?>index.php/EditItem/edit_product" method="post"
              enctype="multipart/form-data">
            <?php foreach ($products->result() as $row) { ?>
                <input type="text" value="<?php echo $row->id ?>" name="product_id" style="display: none">
                <div class="col-md-6">
                    <div class="text-center">
                        <img src="<?php echo $row->imageurl ?>" alt="" width="30%" id="set_product_picture">
                    </div>
                    <br>
                    <input type="file" name="product-image" id="product-image"
                           onchange="Picture_Preview(this,'set_product_picture')" style="display: none">
                    <div class="form-group">
                        <button type="button" class="btn btn-primary form-control"
                                onclick="select_file('product-image')">Change Product Image
                        </button>
                    </div>
                    <div class="form-group">
                        <label>Name:</label>
                        <input type="text" name="product-name" value="<?php echo $row->productname ?>"
                               class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Description:</label>
                        <textarea type="text" name="product-description" class="form-control" rows="5"
                                  required><?php echo $row->description ?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Ingredients:</label>
                        <textarea type="text" name="product-ingredient" class="form-control" rows="5"
                                  required><?php echo $row->ingredients ?></textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="text-center">
                        <img src="<?php echo $row->nutritionfileurl ?>" alt="" width="30%" id="set_nutrition_picture">
                    </div>
                    <br>
                    <input type="file" name="nutrition-image" id="nutrition-image"
                           onchange="Picture_Preview(this,'set_nutrition_picture')" style="display: none">
                    <div class="form-group">
                        <button type="button" class="btn btn-primary form-control"
                                onclick="select_file('nutrition-image')">Change Nutrition Image
                        </button>
                    </div>
                    <div class="form-group">
                        <label>Allergens</label>
                        <textarea type="text" name="product-allergen" class="form-control" rows="5"
                                  required><?php echo $row->allergens ?></textarea>
                    </div>
                    <div class="form-group">
                        <select name="product-category" id="product-category" onmouseover="product_category()"
                                class="form-control">
                            <option selected>Select Category</option>
                        </select>
                        <?php $category_url = base_url() . "index.php/AddItem/set_category";
                        echo "<button type=\"button\" class=\"btn btn-primary\" onclick=\"addlist('category-list','product-category','$category_url','category')\">Add</button>";
                        ?>
                    </div>
                    <ul id="category-list">
                        <?php if (!empty($category)): ?>
                            <?php
                            $categoryids = array();
                            foreach ($category->result() as $row) {
                                $btnid ='id=cat-'.$row->id;
                                $listid = 'category-'.$row->id;
                                echo "<li id='$listid'><div class='row' style='margin-top:3px;'><span style='border-bottom:2px solid darkblue;'><div class='col-md-9'> ".$row->id .".".$row->categoryname."</div><div class='col-md-2'><button type='button' class='btn btn-danger'".$btnid." onclick='delete_list(this.id)'>Remove</button></div></span></div></li>";
                                array_push($categoryids, $row->id);
                            }
                            $this->session->set_userdata('category_list', $categoryids);
                            $this->session->set_userdata('old_category_list', $categoryids);
                            ?>
                        <?php endif; ?>
                    </ul>
                    <div class="form-group">
                        <select name="recipe-suggestion" class="form-control" onmouseover="recipe_suggestion()"
                                id="recipesuggestion">
                            <option value="0" selected>Recipe Suggestion</option>
                        </select>
                        <?php
                        $recipe_url = base_url() . "index.php/AddItem/set_recipes";
                        echo "<button type=\"button\" class=\"btn btn-primary\" onclick=\"addlist('recipe-list','recipesuggestion','$recipe_url','recipe')\">Add</button>";
                        ?>
                    </div>
                    <ul id="recipe-list">
                        <?php if (!empty($recipes)): ?>
                            <?php
                            $recipeids = array();
                            foreach ($recipes->result() as $row) {
                                $btnid ='id=rec-'.$row->id;
                                $listid = 'recipe-'.$row->id;
                                echo "<li id='$listid'><div class='row' style='margin-top:3px;'><span style='border-bottom:2px solid darkblue;'><div class='col-md-9'> ".$row->id .".".$row->title."</div><div class='col-md-2'><button type='button' class='btn btn-danger'".$btnid." onclick='delete_list(this.id)'>Remove</button></div></span></div></li>";
                                array_push($recipeids, $row->id);
                            }
                            $this->session->set_userdata('recipe_list', $recipeids);
                            $this->session->set_userdata('old_recipe_list', $recipeids);
                            ?>
                        <?php endif; ?>
                    </ul>
                    <div class="form-group">
                        <select name="awards-suggestion" class="form-control" onmouseover="awards_suggestion()"
                                id="awards-suggestion">
                            <option value="0" selected>Add Awards</option>
                        </select>
                        <?php $awards_url = base_url() . "index.php/AddItem/set_awards";
                        echo "<button type=\"button\" class=\"btn btn-primary\" onclick=\"addlist('award-list','awards-suggestion','$awards_url','award')\">Add</button>"
                        ?>

                    </div>
                    <ul id="award-list">
                        <?php if (!empty($awards)): ?>
                            <?php
                            $awardids = array();
                            foreach ($awards->result() as $row) {
                                $btnid ='id=awa-'.$row->id;
                                $listid = 'award-'.$row->id;
                                echo "<li id='$listid'><div class='row' style='margin-top:3px;'><span style='border-bottom:2px solid darkblue;'><div class='col-md-9'> ".$row->id .".".substr($row->description,0,50)."</div><div class='col-md-2'><button type='button' class='btn btn-danger'".$btnid." onclick='delete_list(this.id)'>Remove</button></div></span></div></li>";
                                array_push($awardids, $row->id);
                            }
                            $this->session->set_userdata('award_list', $awardids);
                            $this->session->set_userdata('old_award_list', $awardids);
                            ?>
                        <?php endif; ?>
                    </ul>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Update Product">
                    </div>
                    <br>
                </div>
            <?php } ?>
        </form>
    </div>
</div>