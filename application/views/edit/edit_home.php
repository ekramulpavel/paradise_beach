<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        EDIT HOMES
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/EditItem/edit_home" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <?php foreach ($homes->result() as $row){?>
                    <input type="text" value="<?php echo $row->id?>" name="home_id" style="display: none">
                <div class="text-center">
                    <img src="<?php echo $row->imageurl;?>" width="30%" id="set_home_image">
                </div>
                <br>
                <input type="file" name="home-image" id="home-image"
                       onchange="Picture_Preview(this,'set_home_image')" style="display: none">
                <div class="form-group">
                    <button type="button" class="btn btn-primary form-control"
                            onclick="select_file('home-image')">Change Home Image
                    </button>
                </div>
                <div class="form-group">
                    <label>Title:</label>
                    <input type="text" name="home-title" value="<?php echo $row->title;?>" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Subtitle:</label>
                    <input type='text' class="form-control" name="home-subtitle" value="<?php echo $row->subtitle;?>">
                </div>
                <div class="form-group">
                    <label>Link:</label>
                    <input type='text' class="form-control" name="home-link" value="<?php if(!empty($row->link)){echo $row->link;}?>"  placeholder="https://www.paradisebeach.ws/...">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Update Home">
                </div>
                <?php }?>
            </div>
        </form>
    </div>
</div>