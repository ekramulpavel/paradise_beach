<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        EDIT BANNER
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/EditItem/edit_banner" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <?php foreach ($banners->result() as $row){?>
                    <input type="text" value="<?php echo $row->id?>" name="banner_id" style="display: none">
                <div class="text-center">
                    <img src="<?php echo $row->imageurl?>" width="30%" id="set_banner_image">
                </div>
                <br>
                <input type="file" name="banner-image" id="banner-image"
                           onchange="Picture_Preview(this,'set_banner_image')" style="display: none">
                <div class="form-group">
                    <button type="button" class="btn btn-primary form-control"
                                onclick="select_file('banner-image')">Change Banner Image
                    </button>
                </div>
                <div class="form-group">
                    <label>Description:</label>
                    <textarea class="form-control" name="banner-description" rows="5"><?php echo $row->description?></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Update Banner">
                </div>
                <?php }?>
            </div>
        </form>
    </div>
</div>