<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        EDIT TESTIMONIAL
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/EditItem/edit_testimonial" method="post" enctype="multipart/form-data">
            <?php foreach ($testimonials->result() as $row){?>
                <input type="text" value="<?php echo $row->id?>" name="testimonial_id" style="display: none">
            <div class="col-md-6">
                <div class="text-center">
                    <img src="<?php echo $row->imageurl;?>" width="30%" id="set_testimonial_image">
                </div>
                <br>
                <input type="file" style="display: none" name="upload-testimonial-image" id="upload-testimonial-image" onchange="Picture_Preview(this,'set_testimonial_image')" accept="image/*">
                <div class="form-group">
                    <button type="button" class="btn btn-primary form-control"
                            onclick="select_file('upload-testimonial-image')">
                        Upload Image
                    </button>
                </div>
                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" name="name" value="<?php echo $row->name?>" class="form-control">
                </div>
                <div class="form-group">
                    <label>Description:</label>
                    <textarea class="form-control" name="description" rows="5"><?php echo $row->description;?></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Update Testimonial">
                </div>
            </div>
            <?php }?>
        </form>
    </div>
</div>