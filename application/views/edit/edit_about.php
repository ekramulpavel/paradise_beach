<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        EDIT ABOUT US SECTION
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/EditItem/edit_about" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <?php foreach ($about->result() as $row){?>
                    <input type="text" value="<?php echo $row->id?>" name="about_id" style="display: none">
                <div class="text-center">
                    <img src="<?php echo $row->imageurl;?>" width="30%" id="set_about_image">
                </div>
                <br>
                <input type="file" name="about-image" id="about-image"
                       onchange="Picture_Preview(this,'set_about_image')" style="display: none">
                <div class="form-group">
                    <button type="button" class="btn btn-primary form-control"
                            onclick="select_file('about-image')">Change About Image
                    </button>
                </div>
                <div class="form-group">
                    <label>Description:</label>
                    <textarea class="form-control" name="about-description" rows="5"><?php echo $row->description;?></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Update About">
                </div>
                <?php }?>
            </div>
        </form>
    </div>
</div>