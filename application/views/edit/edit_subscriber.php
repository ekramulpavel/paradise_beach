<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        EDIT SUBSCRIBER EMAIL
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/EditItem/edit_subscriber" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <?php foreach ($subscriber->result() as $row){?>
                    <input type="text" value="<?php echo $row->id?>" name="subscriber_id" style="display: none">
                <div class="form-group">
                    <label>Email:</label>
                    <input class="form-control" name="email" value="<?php echo $row->email;?>">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Update">
                </div>
                <?php }?>
            </div>
        </form>
    </div>
</div>