<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        EDIT STORE
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/EditItem/edit_store" method="post" enctype="multipart/form-data">
            <?php foreach ($stores->result() as $row){?>
                <input type="text" value="<?php echo $row->id?>" name="store_id" style="display: none">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" name="store-name" value="<?php echo $row->name?>" class="form-control">
                </div>
                <div class="form-group">
                    <label>Address:</label>
                    <input type="text" name="store-address" value="<?php echo $row->address?>" class="form-control">
                </div>
                <div class="form-group">
                    <label>District:</label>
                    <input type="text" name="store-district" value="<?php echo $row->district?>" class="form-control">
                </div>
                <div class="form-group">
                    <label>State:</label>
                    <input type="text" name="store-state" value="<?php echo $row->state?>" class="form-control">
                </div>
                <div class="form-group">
                    <label>Post Code:</label>
                    <input type="text" name="store-postcode" value="<?php echo $row->post_code?>" class="form-control">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Update Store">
                </div>
            </div>
            <?php }?>
        </form>
    </div>
</div>