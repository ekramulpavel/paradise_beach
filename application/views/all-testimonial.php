<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        TESTIMONIAL
        </span>
    </div>
</div>
<br>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Image</th>
            <th>Name</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $url = base_url()."index.php/EditItem/edit_testimonial_view";
        foreach ($testimonials->result() as $row)
        {
            echo "<tr>";
            echo "<td>".$row->id."</td>";
            echo "<td style='width: 20%'><img src='$row->imageurl' width='80%'></td>";
            echo "<td style='width: 20%'>$row->name</td>";
            echo "<td style='width: 20%'>$row->description</td>";;
            echo "<td style='width: 20%'><a class='btn btn-primary' href='$url/$row->id'>Edit</a>
                       <button class='btn btn-danger' onclick='delete_store($row->id)' >Delete</button>
                  </td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
</div>