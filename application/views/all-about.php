<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        ABOUT US SECTION LIST
        </span>
    </div>
</div>
<br>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>About Us Image</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $url = base_url()."index.php/EditItem/edit_about_view";
        foreach ($about->result() as $row)
        {
            echo "<tr>";
            echo "<td>".$row->id."</td>";
            echo "<td style='width: 25%'><img src='$row->imageurl' width='50%'></td>";
            echo "<td>$row->description</td>";
            echo "<td style='width: 20%'><a class='btn btn-primary' href='$url/$row->id'>Edit</a>
                       <button class='btn btn-danger' onclick='delete_about($row->id)' >Delete</button>
                  </td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
</div>