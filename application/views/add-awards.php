<div class="container-fluid">
    <div class="col-md-12">
        <span style="border-bottom:3px solid #ff6600;font-size:24px;font-weight:400;font-family: 'Droid Serif', serif;"> 
        ADD AWARDS
        </span>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="col-md-12">
        <form action="<?php echo base_url()?>index.php/AddItem/add_award" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <input type="file" style="display: none" name="upload-award-image" id="upload-award-image" onchange="display_filename(this.id,'display_award')" accept="image/*" required>
                <div class="form-group">
                    <label>Image:</label><label id="display_award" style="margin-left: 10px"></label>
                    <button type="button" class="btn btn-primary form-control"
                            onclick="select_file('upload-award-image')">
                        Upload Image
                    </button>
                </div>
                <div class="form-group">
                    <label>Date:</label>
                    <input type="text" name="award-date" id="award-date" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Description:</label>
                    <textarea class="form-control" name="award-description" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save">
                </div>
            </div>
        </form>
    </div>
</div>