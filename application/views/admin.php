<div class="logo text-center">
    <img src="<?php echo base_url() ?>assets/image/paradisebeach-log.jpg" alt="logo" class="logo logo-display"
         width="15%">
</div>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: #fff;">
                    <?php echo $this->session->userdata('useremail')?>
                    <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url()?>index.php/admin/logout"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3" id="sidemenu">
            <div id="sidenav1">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#sideNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="sideNavbar">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="<?php echo base_url() ?>index.php/admin">
                                        <span class="glyphicon glyphicon-book"></span>
                                        Dashboard
                                    </a>
                                </h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne">
                                        <span class="glyphicon glyphicon-home">
                                        </span>
                                        Home
                                        <span class="caret"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse">
                                <ul class="list-group">
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/all_banner" class="navlink">
                                            All Banners
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/add_banner" class="navlink">
                                            Add New Banner
                                        </a>
                                    </li>
                                                                        <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/all_home" class="navlink">
                                            All Homes
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/add_home" class="navlink">
                                            Add New Home
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseTwo">
                                        <span class="glyphicon glyphicon-briefcase"> </span>
                                        Products
                                        <span class="caret"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <ul class="list-group">
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin" class="navlink">
                                            All Products
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/add_product" class="navlink">
                                            Add New Product
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/all_category" class="navlink">
                                            All Categories
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/add_category" class="navlink">Add
                                            New Category
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseThree">
                                        <span class="glyphicon glyphicon-fire">
                                        </span>
                                        Recipe
                                        <span class="caret"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <ul class="list-group">
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/all_recipes" class="navlink">
                                            All Recipes
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/add_recipe" class="navlink">
                                            Add New Recipe
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseSix">
                                        <span class="glyphicon glyphicon-fire">
                                        </span>
                                        Stores
                                        <span class="caret"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse">
                                <ul class="list-group">
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/all_store" class="navlink">
                                            All Stores
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/add_store" class="navlink">
                                            Add New Store
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseTen">
                                        <span class="glyphicon glyphicon-fire">
                                        </span>
                                        Testimonials
                                        <span class="caret"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTen" class="panel-collapse collapse">
                                <ul class="list-group">
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/all_testimonial" class="navlink">
                                            All Testimonial
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/add_testimonial" class="navlink">
                                            Add New Testimonial
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseFour">
                                        <span class="glyphicon glyphicon-education">
                                        </span>
                                        Awards
                                        <span class="caret"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse">
                                <ul class="list-group">
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/all_awards" class="navlink">
                                            All Awards
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/add_award" class="navlink">
                                            Add New Awards
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseFive">
                                        <span class="glyphicon glyphicon-user">
                                        </span>
                                        About Us
                                        <span class="caret"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse">
                                <ul class="list-group">
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/all_about" class="navlink">
                                            All Section
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>index.php/admin/add_aboutus" class="navlink">
                                            Add New Section
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="<?php echo base_url() ?>index.php/admin/contactinfo">
                                        <span class="glyphicon glyphicon-phone">
                                        </span>
                                        Contact
                                    </a>
                                </h4>
                            </div>
                        </div>
                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="<?php echo base_url() ?>index.php/admin/all_subscribers">
                                        <span class="glyphicon glyphicon-envelope">
                                        </span>
                                        Subscribers
                                    </a>
                                </h4>
                            </div>
                        </div>
                        <!--End of Admin Sidebar-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <?php $this->load->view($partial_view); ?>
        </div>
    </div>
</div>
