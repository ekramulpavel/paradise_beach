<?php
/**
 * Created by PhpStorm.
 * User: ashikmahmud
 * Date: 04/09/2017
 * Time: 20:26
 */

class DeleteItem_Model extends CI_Model
{

    public function delete_product($id)
    {
        $this->db->where('id',$id);
        $result = $this->db->delete('products');

        return $result;
    }
    public function delete_recipe($id)
    {
        $this->db->where('id',$id);
        $result = $this->db->delete('recipe');

        return $result;
    }
    public function delete_award($id)
    {
        $this->db->where('id',$id);
        $result = $this->db->delete('awards');

        return $result;
    }
    public function delete_home($id)
    {
        $this->db->where('id',$id);
        $result = $this->db->delete('home');

        return $result; 
    }
    public function delete_banner($id)
    {
        $this->db->where('id',$id);
        $result = $this->db->delete('banner');

        return $result;
    }
    public function delete_category($id)
    {
        $this->db->where('id',$id);
        $result = $this->db->delete('category');

        return $result;
    }
    public function delete_about($id)
    {
        $this->db->where('id',$id);
        $result = $this->db->delete('about');

        return $result;
    }
    public function delete_subscriber($id)
    {
        $this->db->where('id',$id);
        $result = $this->db->delete('subscribers');
        
        return $result;
    }
}