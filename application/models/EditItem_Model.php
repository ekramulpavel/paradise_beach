<?php
/**
 * Created by PhpStorm.
 * User: ashikmahmud
 * Date: 05/09/2017
 * Time: 11:31
 */

class EditItem_Model extends CI_Model
{
    public function get_award($id)
    {
        $this->db->select();
        $this->db->from('awards');
        $this->db->where('id',$id);
        $result = $this->db->get();
        return $result;
    }
    public function get_banner($id)
    {
        $this->db->select();
        $this->db->from('banner');
        $this->db->where('id',$id);
        $result = $this->db->get();
        return $result;
    }
    public function get_subscriber($id)
    {
        $this->db->where('id',$id);
        $result = $this->db->get('subscribers');
        return $result;
    }
    public function get_recipe($id)
    {
        $this->db->select();
        $this->db->from('recipe');
        $this->db->where('id',$id);
        $result = $this->db->get();
        return $result;
    }
    public function get_home($id)
    {
        $this->db->select();
        $this->db->from('home');
        $this->db->where('id',$id);
        $result = $this->db->get();
        return $result; 
    }
    public function get_product($id)
    {
        $this->db->select();
        $this->db->from('products');
        $this->db->where('id',$id);
        $result = $this->db->get();
        return $result;
    }
    public function get_about($id)
    {
        $this->db->select();
        $this->db->from('about');
        $this->db->where('id',$id);
        $result = $this->db->get();
        return $result;  
    }
    public function get_category($id)
    {
        $this->db->select();
        $this->db->from('category');
        $this->db->where('id',$id);
        $result = $this->db->get();

        $this->db->select();
        $all_category = $this->db->get('category');

        $special_category = array(
            'category' => $result,
            'all_category' => $all_category
        );
        return $special_category;
    }

    public function edit_award($award_data)
    {
        $award_id =$this->input->post('award_id');

        $this->db->where('id',$award_id);
        $result = $this->db->update('awards',$award_data);

        return $result;
    }
    public function edit_home($home_data)
    {
        $home_id =$this->input->post('home_id');
        $this->db->where('id',$home_id);
        $result = $this->db->update('home',$home_data);

        return $result; 
    }
    public function edit_banner($banner_data)
    {
        $banner_id = $this->input->post('banner_id');

        $this->db->where('id',$banner_id);
        $result = $this->db->update('banner',$banner_data);

        return $result;
    }
    public function edit_recipe($recipe_data)
    {
        $recipe_id = $this->input->post('recipe_id');
        $this->db->where('id',$recipe_id);
        $result = $this->db->update('recipe',$recipe_data);

        return $result;
    }
    public function edit_category($category_data)
    {
        $category_id = $this->input->post('category_id');

        $this->db->where('id',$category_id);
        $result = $this->db->update('category',$category_data);

        return $result;
    }
    public function edit_about($about_data)
    {
        $about_id = $this->input->post('about_id');

        $this->db->where('id',$about_id);
        $result = $this->db->update('about',$about_data);

        return $result;
    }
    public function edit_subscriber($data)
    {
        $subscriber_id = $this->input->post('subscriber_id');

        $this->db->where('id',$subscriber_id);
        $result = $this->db->update('subscribers',$data);
        
        return $result;
        
    }
    public function edit_contact($contact_data)
    {
        $contact_id = $this->input->post('contact_id');
        $this->db->where('id',$contact_id);
        $result = $this->db->update('contact',$contact_data);
        
        return $result;
    }
    public function edit_product($product_data)
    {
        $this->db->trans_begin();
        $product_id = $this->input->post('product_id');

        $this->db->where('id',$product_id);
        $this->db->update('products',$product_data);
        
        
        
        //Start edit recipe section
        
        if ($this->session->userdata('old_recipe_list'))
        {
            $old_recipe_list = $this->session->userdata('old_recipe_list');
            $new_recipe_list = $this->session->userdata('recipe_list');
            
            if(count($new_recipe_list) >count($old_recipe_list))
            {
                foreach ($new_recipe_list as $key => $value)
                {
                    if (!in_array($new_recipe_list[$key],$old_recipe_list))
                    {
                        $recipe_suggestion = array(
                            'productid' => $product_id,
                            'recipeid' => $new_recipe_list[$key]
                        );
                        $this->db->insert('productrecipesuggestion',$recipe_suggestion);
                    }
                }
            }
            elseif(count($new_recipe_list) < count($old_recipe_list))
            {
                foreach ($old_recipe_list as $key => $value)
                {
                    if (!in_array($old_recipe_list[$key],$new_recipe_list))
                    {
                        $this->db->where('productid',$product_id);
                        $this->db->where('recipeid', $old_recipe_list[$key]);
                        $this->db->delete('productrecipesuggestion');
                    }
                }
            }
            elseif(count($new_recipe_list) == count($old_recipe_list))
            {
                foreach ($old_recipe_list as $key => $value)
                {
                    if (!in_array($old_recipe_list[$key],$new_recipe_list))
                    {
                        $this->db->where('productid',$product_id);
                        $this->db->where('recipeid', $old_recipe_list[$key]);
                        $this->db->delete('productrecipesuggestion');
                    }
                }
                foreach ($new_recipe_list as $key => $value)
                {
                    if (!in_array($new_recipe_list[$key],$old_recipe_list))
                    {
                        $recipe_suggestion = array(
                            'productid' => $product_id,
                            'recipeid' => $new_recipe_list[$key]
                        );
                        $this->db->insert('productrecipesuggestion',$recipe_suggestion);
                    }
                }
            }
        }
        else
        {
            if ($this->session->userdata('recipe_list'))
            {
                $recipe_list = $this->session->userdata('recipe_list');
                foreach ($recipe_list as $row)
                {
                    $recipe_suggestion_list = array(
                        'productid' => $product_id,
                        'recipeid' => $row
                    );
                    $this->db->insert('productrecipesuggestion',$recipe_suggestion_list);
                }
            }
        }
        
        //end edit recipe section
        
        
        
        //Start Edit Product Category
        if ($this->session->userdata('old_category_list'))
        {
            $old_category_list = $this->session->userdata('old_category_list');
            $new_category_list = $this->session->userdata('category_list');
            
            if(count($new_category_list) >count($old_category_list))
            {
                foreach ($new_category_list as $key => $value)
                {
                    if (!in_array($new_category_list[$key],$old_category_list))
                    {
                        $category_suggestion = array(
                            'productid' => $product_id,
                            'categoryid' => $new_category_list[$key]
                        );
                        $this->db->insert('productcategory',$category_suggestion);
                    }
                }
            }
            elseif(count($new_category_list) < count($old_category_list))
            {
                foreach ($old_category_list as $key => $value)
                {
                    if (!in_array($old_category_list[$key],$new_category_list))
                    {
                        $this->db->where('productid',$product_id);
                        $this->db->where('categoryid', $old_category_list[$key]);
                        $this->db->delete('productcategory');
                    }
                }
            }
            elseif(count($new_category_list) == count($old_category_list))
            {
                foreach ($old_category_list as $key => $value)
                {
                    if (!in_array($old_category_list[$key],$new_category_list))
                    {
                        $this->db->where('productid',$product_id);
                        $this->db->where('categoryid', $old_category_list[$key]);
                        $this->db->delete('productcategory');
                    }
                }
                foreach($new_category_list as $key => $value)
                {
                    if (!in_array($new_category_list[$key],$old_category_list))
                    {
                        $category_suggestion = array(
                            'productid' => $product_id,
                            'categoryid' => $new_category_list[$key]
                        );
                        $this->db->insert('productcategory',$category_suggestion);
                    }
                }
            }
        }
        else
        {
            if ($this->session->userdata('category_list'))
            {
                $category_list = $this->session->userdata('category_list');
                foreach ($category_list as $row)
                {
                    $category_suggestion_list = array(
                        'productid' => $product_id,
                        'categoryid' => $row
                    );
                    $this->db->insert('productcategory',$category_suggestion_list);
                }
            }
        }
        //End Edit Product Category
        
        
        //Edit Product Award Section Start
        if ($this->session->userdata('old_award_list'))
        {
            $old_award_list = $this->session->userdata('old_award_list');
            $new_award_list = $this->session->userdata('award_list');
            
            if(count($new_award_list) > count($old_award_list))
            {
                foreach ($new_award_list as $key=> $value)
                {
                    if (!in_array($new_award_list[$key],$old_award_list))
                    {
                        $product_award = array(
                            'productid' => $product_id,
                            'awardid' => $new_award_list[$key]
                        );
                        $this->db->insert('productawards',$product_award);
                    }
                }
            }
            elseif(count($new_award_list) < count($old_award_list))
            {
                foreach ($old_award_list as $key => $value)
                {
                    if (!in_array($old_award_list[$key],$new_award_list))
                    {
                        $this->db->where('productid',$product_id);
                        $this->db->where('awardid', $old_award_list[$key]);
                        $this->db->delete('productawards');
                    }
                }
            }
            elseif(count($new_award_list) == count($old_award_list))
            {
                foreach ($old_award_list as $key => $value)
                {
                    if (!in_array($old_award_list[$key],$new_award_list))
                    {
                        $this->db->where('productid',$product_id);
                        $this->db->where('awardid', $old_award_list[$key]);
                        $this->db->delete('productawards');
                    }
                }
                
                foreach ($new_award_list as $key=> $value)
                {
                    if (!in_array($new_award_list[$key],$old_award_list))
                    {
                        $product_award = array(
                            'productid' => $product_id,
                            'awardid' => $new_award_list[$key]
                        );
                        $this->db->insert('productawards',$product_award);
                    }
                }
            }
        }
        else
        {
            if ($this->session->userdata('award_list'))
            {
                $award_list = $this->session->userdata('award_list');
                foreach ($award_list as $row)
                {
                    $product_award = array(
                        'productid' => $product_id,
                        'awardid' => $row
                    );
                    $this->db->insert('productawards',$product_award);
                }
            }
        }
        
        //end product award section 
        
        
        
        
        
        if ($this->db->trans_status() === FALSE) {

            $this->db->trans_rollback();
            return false;

        } else {

            $this->db->trans_commit();
            return true;

        }
    }
    public function get_store($id)
    {
        $this->db->where('id',$id);
        $result = $this->db->get('store');
        return $result;
    }
    public function edit_store()
    {
        //Get all the input field
        $store_id = $this->input->post('store_id');
        $store_name = $this->input->post('store-name');
        $store_address = $this->input->post('store-address');
        $store_district = $this->input->post('store-district');
        $store_state = $this->input->post('store-state');
        $store_postcode = $this->input->post('store-postcode');
        
        $store_data = array(
            'name' => $store_name,
            'address' => $store_address,
            'district' => $store_district,
            'state' => $store_state,
            'post_code' => $store_postcode
            );
        $this->db->where('id',$store_id);
        $result = $this->db->update('store',$store_data);
        return $result;
    }
    public function edit_testimonial($testimonial_data)
    {
        $testimonial_id = $this->input->post('testimonial_id');
        $this->db->where('id',$testimonial_id);
        $result = $this->db->update('testimonial',$testimonial_data);
        
        return $result;
    }
}