<?php
/**
 * Created by PhpStorm.
 * User: ashikmahmud
 * Date: 02/09/2017
 * Time: 23:50
 */

class Admin_Model extends CI_Model
{
    public function add_category()
    {
        $category_name = $this->input->post('category-name');
        $parent_category = $this->input->post('parent-category');

        if (!$parent_category == 0) {
            $category_data = array(
                'categoryname' => $category_name,
                'categoryid' => $parent_category
            );
            $result = $this->db->insert('category', $category_data);
            return $result;
        } else {
            $category_data = array(
                'categoryname' => $category_name
            );
            $result = $this->db->insert('category', $category_data);
            return $result;
        }
    }
    public function all_subscribers()
    {
        $result = $this->db->get('subscribers');
        return $result;
    }
    public function subscribe_newsletter()
    {
        $email = $this->input->post('email');
        $this->db->where('email',$email);
        $email_exsist = $this->db->get('subscribers')->result();
        if(count($email_exsist) ==0 )
        {
            $subscribe_data = array(
                'email' => $email
                );
            $result = $this->db->insert('subscribers',$subscribe_data);
            return $result;
        }
        else
        {
            return true;
        }
    }
    public function login_user($useremail,$password)
    {
        $this->db->where('email',$useremail);

        $user = $this->db->get('admin');

        $db_password = $user->row(2)->password;

        if(strcmp($password , $db_password) == 0)
        {
            $userid = $user->row(0)->id;
            $data = array(
                'user_id' => $user->row(0)->id,
            );

            return $data;
        }
        else
        {
            return false;
        }
    }
    public function get_category()
    {
        $this->db->select();
        $this->db->from('category');

        $result = $this->db->get();
        return $result;
    }
    public function get_product_category()
    {
        $this->db->select('c.id,c.categoryname,c.categoryid,pca.productid');
        $this->db->from('productcategory pca');
        $this->db->join('category c','pca.categoryid=c.id','both');
        $result = $this->db->get();
        
        return $result;
    }
    public function add_banner($image, $imageurl)
    {
        $banner_description = $this->input->post('banner-description');

        $banner_data = array(
            'image' => $image,
            'imageurl' => $imageurl,
            'description' => $banner_description
        );
        $result = $this->db->insert('banner', $banner_data);
        return $result;
    }
    public function get_contact()
    {
        $this->db->select();
        $result = $this->db->get('contact');
        return $result;
    }
    public function get_banner()
    {
        $this->db->select();
        $this->db->from('banner');

        $result = $this->db->get();
        return $result;
    }
    public function add_home($image,$imageurl)
    {
        $home_title = $this->input->post('home-title');
        $home_subtitle = $this->input->post('home-subtitle');
        
        $home_data = array(
            'title' => $home_title,
            'subtitle' => $home_subtitle,
            'image' => $image,
            'imageurl' => $imageurl
            );
        $result = $this->db->insert('home',$home_data);
        return $result;
    }
    public function get_homes()
    {
        $this->db->select();
        $this->db->from('home');
        $result = $this->db->get();
        return $result; 
    }
    public function get_testimonial()
    {
        $this->db->select();
        $this->db->from('testimonial');
        $result = $this->db->get();
        return $result;  
    }
    public function add_recipe($image, $image_url)
    {
        $recipe_title = $this->input->post('recipe-title');
        $recipe_ingredient = $this->input->post('recipe-ingredient');
        $recipe_procedure = $this->input->post('recipe-procedure');

        $recipe_data = array(
            'title' => $recipe_title,
            'filename' => $image,
            'imageurl' => $image_url,
            'ingredient' => $recipe_ingredient,
            'recipe_procedure' => $recipe_procedure
        );

        $result = $this->db->insert('recipe', $recipe_data);
        return $result;
    }

    public function get_recipes()
    {
        $this->db->select('r.id,r.title,r.imageurl,r.ingredient,p.productname');
        $this->db->from('recipe r');
        $this->db->join('productrecipesuggestion prs', ' r.id=prs.recipeid','left');
        $this->db->join('products p', ' p.id=prs.productid ','left');
        $result = $this->db->get();
        return $result;
    }
    public function get_all_recipe()
    {
        $this->db->select();
        $result = $this->db->get('recipe');
        return $result;
    }

    public function add_award($image, $image_url)
    {
        $award_date = $this->input->post('award-date');
        $award_description = $this->input->post('award-description');
        $format_awarddate = date("Y-m-d", strtotime($award_date));


        $award_data = array(
            'filename' => $image,
            'imageurl' => $image_url,
            'awardyear' => $format_awarddate,
            'description' => $award_description
        );
        $result = $this->db->insert('awards', $award_data);

        return $result;

    }
    public function ajax_product()
    {
        $id = $this->input->post('id');
        if($id !='0')
        {
            $products_array = array();
            $this->db->select('productid');
            $this->db->where('categoryid',$id);
            $result = $this->db->get('productcategory');
            
            foreach($result->result() as $row)
            {
                array_push($products_array,$row->productid);
            }
            $this->db->select();
            $this->db->where_in('id',$products_array);
            $products = $this->db->get('products');
            return $products;
        }
        else
        {
            $result = $this->db->get('products');
            return $result;
        }
    }
    public function sort_product()
    {
        $sortoption = $this->input->post('sort');
        
        $this->db->from('products');
        if(strcmp($sortoption,'atoz') == 0)
        {
            $this->db->order_by("productname", "asc");
        }
        else
        {
            $this->db->order_by("productname", "desc");
        }
        $query = $this->db->get(); 
        return $query;
    }
    public function get_awards()
    {
        $this->db->select('a.id,a.imageurl,a.description,a.awardyear,p.productname');
        $this->db->from('awards a');
        $this->db->join('productawards pa', 'pa.awardid = a.id','left');
        $this->db->join('products p', 'pa.productid = p.id','left');
        $this->db->order_by('awardyear','ASC');
        $result = $this->db->get();
        return $result;
    }
    public function add_about($about_data)
    {
        $result = $this->db->insert('about',$about_data);
        return $result;
    }
    public function get_about()
    {
        $this->db->select();
        $result = $this->db->get('about');
        return $result;
    }
    public function add_product($product_image, $p_imageurl, $nutrition_image, $n_imageurl)
    {
        $this->db->trans_begin();
        $product_name = $this->input->post('product-name');
        $product_description = $this->input->post('product-description');
        $product_ingredient = $this->input->post('product-ingredient');
        $product_allergen = $this->input->post('product-allergen');

        $product_data = array(
            'productname' => $product_name,
            'productimage' => $product_image,
            'imageurl' => $p_imageurl,
            'nutritionfile' => $nutrition_image,
            'nutritionfileurl' => $n_imageurl,
            'description' => $product_description,
            'ingredients' => $product_ingredient,
            'allergens' => $product_allergen

        );
        $this->db->insert('products', $product_data);

        $product_id = $this->productid();
        if ($this->session->userdata('recipe_list')) {
            $recipe_list = $this->session->userdata('recipe_list');
            foreach ($recipe_list as $recipeid) {
                $recipe_suggestion = array(
                    'productid' => $product_id,
                    'recipeid' => $recipeid
                );
                $this->db->insert('productrecipesuggestion', $recipe_suggestion);
            }
        }
        if ($this->session->userdata('award_list')) {
            $award_list = $this->session->userdata('award_list');
            foreach ($award_list as $awardid) {
                $product_awards = array(
                    'productid' => $product_id,
                    'awardid' => $awardid
                );
                $this->db->insert('productawards', $product_awards);
            }
        }
        if ($this->session->userdata('category_list')) {
            $category_list = $this->session->userdata('category_list');
            foreach ($category_list as $categoryid) {
                $product_category = array(
                    'productid' => $product_id,
                    'categoryid' => $categoryid
                );
                $this->db->insert('productcategory', $product_category);
            }
        }
        if ($this->db->trans_status() === FALSE) {

            $this->db->trans_rollback();
            return false;

        } else {

            $this->db->trans_commit();
            return true;

        }
    }

    private function productid()
    {
        $this->db->select_max('id');
        $result = $this->db->get('products')->row();
        return $result->id;
    }

    public function get_products()
    {
        $this->db->select();
        $this->db->from('products');
        $result = $this->db->get();
        return $result;
    }

    public function get_product($keyword)
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->like('productname', $keyword, 'both');
        $this->db->or_like('description', $keyword, 'both');
        $result = $this->db->get();
        return $result;
    }
    public function search_products($keyword)
    {
        $this->db->select();
        $this->db->from('products');
        $this->db->like('productname', $keyword, 'both');
        $this->db->or_like('description', $keyword, 'both');
        $result = $this->db->get();
        return $result;
    }

    function get_all_products()
    {
        $this->db->select();
        $this->db->from('products');
        $result = $this->db->get();
        return $result;
    }
    public function sub_category($id)
    {
        $this->db->select('id,categoryname,categoryid');
        $this->db->where('categoryid',$id);
        $result = $this->db->get('category');
        return $result;
    }
    public function product_detail($id)
    {

        $this->db->where('id', $id);
        $product = $this->db->get('products');
        $recipes = null;
        $awards = null;
        $category = null;

        $product_recipe_ids = array();
        $product_award_ids = array();
        $product_category_id = array();

        $this->db->select('recipeid');
        $this->db->from('productrecipesuggestion');
        $this->db->where('productid', $id);
        $recipesid = $this->db->get();


        if (!empty($recipesid->result())) {


            foreach ($recipesid->result() as $row) {
                array_push($product_recipe_ids, $row->recipeid);
            }

            $this->db->select();
            $this->db->where_in('id', $product_recipe_ids);
            $this->db->from('recipe');
            $recipes = $this->db->get();
        }

        $this->db->select('awardid');
        $this->db->from('productawards');
        $this->db->where('productid', $id);
        $awardsid = $this->db->get()->result();

        if (count($awardsid) !=0)
        {
            foreach ($awardsid as $row) {
                array_push($product_award_ids, $row->awardid);
            }

            $this->db->select('*');
            $this->db->where_in('id', $product_award_ids);
            $this->db->from('awards');
            $awards = $this->db->get();
        }
        
        $this->db->select('categoryid');
        $this->db->from('productcategory');
        $this->db->where('productid', $id);
        $categoryid = $this->db->get()->result();
        
        if (count($categoryid) !=0)
        {
            foreach ($categoryid as $row) {
                array_push($product_category_id, $row->categoryid);
            }

            $this->db->select('*');
            $this->db->where_in('id', $product_category_id);
            $this->db->from('category');
            $category = $this->db->get();
        }
        
        $data_array = array(
            'products' => $product,
            'recipes' => $recipes,
            'awards' => $awards,
            'category' => $category
        );
        return $data_array;
    }
    public function recipe_detail($id)
    {
        $result = array();
        $this->db->where('id',$id);
        $recipe = $this->db->get('recipe');
        $result['recipe'] = $recipe;
        
        $recipe_product_ids = array();
        $this->db->select('productid');
        $this->db->from('productrecipesuggestion');
        $this->db->where('recipeid', $id);
        $productids = $this->db->get();
        
        if (!empty($productids->result())) {
            foreach ($productids->result() as $row) {
                array_push($recipe_product_ids, $row->productid);
            }
            $this->db->select();
            $this->db->where_in('id', $recipe_product_ids);
            $this->db->from('products');
            $products = $this->db->get();
            $result['product'] = $products;
        }
        return $result;
    }
    public function add_testimonial($image, $imageurl)
    {
        $name = $this->input->post('name');
        $testimonial_description = $this->input->post('description');

        $testimonial_data = array(
            'name' => $name,
            'description' => $testimonial_description,
            'image' => $image,
            'imageurl' => $imageurl

        );
        $result = $this->db->insert('testimonial', $testimonial_data);
        return $result;
    }
    public function testimonial_detail($id)
    {
        $this->db->where('id',$id);
        $result = $this->db->get('testimonial');
        return $result;
    }
    public function store_detail($id)
    {
        $this->db->where('id',$id);
        $result = $this->db->get('store');
        return $result;
    }
    public function all_store()
    {
        $result = $this->db->get('store');
        return $result;
    }
    public function add_store()
    {
        //Get all the input field
        $store_name = $this->input->post('store-name');
        $store_address = $this->input->post('store-address');
        $store_district = $this->input->post('store-district');
        $store_state = $this->input->post('store-state');
        $store_postcode = $this->input->post('store-postcode');
        
        $store_data = array(
            'name' => $store_name,
            'address' => $store_address,
            'district' => $store_district,
            'state' => $store_state,
            'post_code' => $store_postcode
            );
        $result = $this->db->insert('store',$store_data);
        return $result;
    }
}