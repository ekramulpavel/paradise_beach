<?php


class Home extends CI_Controller
{
    
    public function index()
    {
        $data['main_view'] = 'layout/main-layout';
        $this->load->view('home',$data);
    }
    public function contact()
    {
        $data['main_view'] ="contact";
        $this->load->view('home',$data);
    }
    public function recipies()
    {
        $data['main_view'] ="recipies";
        $this->load->view('home',$data);
    }
    public function products()
    {
        $data['main_view'] ="products";
        $this->load->view('home',$data);
    }
    public function awards()
    {
        $data['main_view'] ="awards";
        $this->load->view('home',$data);
    }
    public function aboutus()
    {
        $data['main_view'] ="aboutus";
        $this->load->view('home',$data);
    }
}