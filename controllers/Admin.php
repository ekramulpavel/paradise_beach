<?php
/**
 * Created by PhpStorm.
 * User: ashikmahmud
 * Date: 01/09/2017
 * Time: 04:45
 */

class Admin extends CI_Controller
{
    public function index()
    {
        $data['products'] = $this->Admin_Model->get_products();
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'all-products';
        $this->load->view('home',$data);
    }
    public function add_product()
    {
        $this->session->unset_userdata('recipe_list');
        $this->session->unset_userdata('award_list');
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'add-new-product';
        $this->load->view('home',$data);
    }
    public function add_recipe()
    {
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'add-recipe';
        $this->load->view('home',$data);
    }
    public function add_award()
    {
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'add-awards';
        $this->load->view('home',$data);
    }
    public function add_aboutus()
    {
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'add-aboutus';
        $this->load->view('home',$data);
    }
    public function contactinfo()
    {
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'contactinfo';
        $this->load->view('home',$data);
    }
    public function add_category()
    {
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'add-new-category';
        $this->load->view('home',$data);
    }
    public function add_banner()
    {
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'add-banner';
        $this->load->view('home',$data);
    }
    public function all_category()
    {
        $data['categories'] = $this->Admin_Model->get_category();
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'categories';
        $this->load->view('home',$data);
    }
    public function all_banner()
    {
        $data['banners'] = $this->Admin_Model->get_banner();
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'all-banners';
        $this->load->view('home',$data);
    }
    public function all_recipes()
    {
        $data['recipes'] = $this->Admin_Model->get_recipes();
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'all-recipes';
        $this->load->view('home',$data);
    }
    public function all_awards()
    {
        $data['awards'] = $this->Admin_Model->get_awards();
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'all-awards';
        $this->load->view('home',$data);
    }
    public function all_products()
    {
        $data['products'] = $this->Admin_Model->get_products();
        $data['main_view'] = 'admin';
        $data['partial_view'] = 'all-products';
        $this->load->view('home',$data);
    }
}