<?php
/**
 * Created by PhpStorm.
 * User: ashikmahmud
 * Date: 02/09/2017
 * Time: 19:49
 */

class AddItem extends CI_Controller
{
    public function add_recipe()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $config['upload_path'] = FCPATH . 'Itemimage/recipe/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['overwrite'] = false;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload('upload-recipe-image');
            $upload_data = $this->upload->data();
            $recipe_image = $upload_data['file_name'];
            $image_url = base_url() . "Itemimage/recipe/" . $recipe_image;

            $inserted_recipe = $this->Admin_Model->add_recipe($recipe_image, $image_url);

            if ($inserted_recipe) {
                redirect('admin/all_recipes');
            }
        }
    }
    public function add_award()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $config['upload_path'] = FCPATH . 'Itemimage/award/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['overwrite'] = false;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload('upload-award-image');
            $upload_data = $this->upload->data();
            $award_image = $upload_data['file_name'];
            $image_url = base_url() . "Itemimage/award/" . $award_image;

            $inserted_award = $this->Admin_Model->add_award($award_image, $image_url);

            if ($inserted_award) {
                redirect('admin/all_awards');
            }
        }
    }
    public function add_banner()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $config['upload_path'] = FCPATH . 'Itemimage/banner/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['overwrite'] = false;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload('upload-banner-image');
            $upload_data = $this->upload->data();
            $banner_image = $upload_data['file_name'];
            $image_url = base_url() . "Itemimage/banner/" . $banner_image;

            $inserted_banner = $this->Admin_Model->add_banner($banner_image, $image_url);

            if ($inserted_banner) {
                redirect('admin/all_banner');
            }
        }

    }
    public function add_aboutus()
    {
        $about_image = $_FILES['upload-about-image']['name'];
        $about_description = $this->input->post('about-description');

        echo $about_description."<br>";
        echo $about_image;
    }
    public function add_product()
    {
        //Uploading Product Image
        $config['upload_path'] = FCPATH . 'Itemimage/product/';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['overwrite'] = false;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('upload-product-image');
        $upload_data = $this->upload->data();
        $product_image = $upload_data['file_name'];
        $product_image_url = base_url() . "Itemimage/product/" . $product_image;


        //Uploading Nutrition Image

        $config['upload_path'] = FCPATH . 'Itemimage/nutrition/';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['overwrite'] = false;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('upload-nutrition-image');
        $upload_data = $this->upload->data();
        $nutrition_image = $upload_data['file_name'];
        $nutrition_image_url = base_url() . "Itemimage/nutrition/" . $nutrition_image;

        $product_inserted = $this->Admin_Model->add_product($product_image,$product_image_url,$nutrition_image,$nutrition_image_url);

        if($product_inserted)
        {
            $this->session->unset_userdata('recipe_list');
            $this->session->unset_userdata('award_list');
            redirect('admin/all_products');
        }

    }
    public function add_category()
    {
        $category_name = $this->input->post('category-name');
        $parent_category = $this->input->post('parent-category');

        $result = $this->Admin_Model->add_category();
        if($result)
        {
            echo $category_name."<br>";
            echo $parent_category;
        }

    }
    public function get_category()
    {
        $category = $this->Admin_Model->get_category();
        $category_list = array(array());

        $i=0;
        foreach ($category->result() as $row)
        {
            $category_list[$i][0]= $row->id;
            $category_list[$i][1]=$row->categoryname;
            $i++;
        }
        echo json_encode($category_list);
    }
    public function get_recipes()
    {
        $recipe = $this->Admin_Model->get_recipes();
        $recipe_list = array(array());

        $i=0;
        foreach ($recipe->result() as $row)
        {
            $recipe_list[$i][0]= $row->id;
            $recipe_list[$i][1]=$row->title;
            $i++;
        }
        echo json_encode($recipe_list);
    }
    public function set_recipes()
    {
        $recipe_id = $this->input->post('id');
        $list_recipes = array();
        if ($this->session->userdata('recipe_list'))
        {
            $found = false;
            $checkList = $this->session->userdata('recipe_list');
            foreach ($checkList as $item)
            {
                if ($item == $recipe_id)
                {
                    $found = true;
                }
            }
            if (!$found)
            {
                array_push($checkList,$recipe_id);
                $this->session->set_userdata('recipe_list',$checkList);
                echo "success";
            }
            else
            {
                echo "Item Already Exist";
            }
        }
        else
        {
            array_push($list_recipes,$recipe_id);
            $this->session->set_userdata('recipe_list',$list_recipes);
            echo "success";
        }

    }
    public function get_awards()
    {
        $award = $this->Admin_Model->get_awards();
        $award_list = array(array());

        $i=0;
        foreach ($award->result() as $row)
        {
            $award_list[$i][0]= $row->id;
            $award_list[$i][1]=$row->description;
            $i++;
        }
        echo json_encode($award_list);
    }
    public function set_awards()
    {
        $award_id = $this->input->post('id');
        $list_awards = array();
        if ($this->session->userdata('award_list'))
        {
            $found = false;
            $checkList = $this->session->userdata('award_list');
            foreach ($checkList as $item)
            {
                if ($item == $award_id)
                {
                    $found = true;
                }
            }
            if (!$found)
            {
                array_push($checkList,$award_id);
                $this->session->set_userdata('award_list',$checkList);
                echo "success";
            }
            else
            {
                echo "Item Already Exist";
            }
        }
        else
        {
            array_push($list_awards,$award_id);
            $this->session->set_userdata('award_list',$list_awards);
            echo "success";
        }
    }
}