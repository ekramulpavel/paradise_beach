-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2018 at 10:02 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paradise_beach`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `imageurl` varchar(500) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `image`, `imageurl`, `description`) VALUES
(1, 'AboutUs_one.jpg', 'http://localhost/Itemimage/aboutus/AboutUs_one.jpg', 'Paradise Beach Purveyors was started by Anmarie Dabinet and Steve Ingram, two ex advertising creatives, born out of their shared food experience and passion for great food, wonderful ingredients and fabulous flavours from all over the world.'),
(6, '', 'http://localhost/Itemimage/aboutus/', 'Anmarie learned about the value of fresh ingredients from her parent’s South Australian vegetable garden and fruit trees and about different cuisines through travel and living in Barcelona for two years. She also ran a successful catering business.'),
(7, 'AboutUs_two1.jpg', 'http://localhost/Itemimage/aboutus/AboutUs_two1.jpg', 'Anmarie’s favourite food experience is wandering from tapas bar to tapas bar in the old quarter of Barcelona late in the evening, enjoying the wonderful flavours with friends.'),
(8, '', 'http://localhost/Itemimage/aboutus/', 'Steve grew up in England, and had food mad parents who would holiday in Europe in summer and dive into every Food Market and Charcuterie and Delicatessen they could find in Spain, France and Italy. He travelled overland to Australia, delighting in every new culinary treat along the way. Street food and casual dining are a particular favourite hence the dips connection.'),
(9, '', 'http://localhost/Itemimage/aboutus/', 'One of his favourite food experiences was on holiday, age 11, meeting an Italian family who took him to their secret wild mushroom and wild strawberry patches high up in the summer meadows of the Dolomite Mountains. They returned to the Hotel having picked several bags full which the proprietor of the hotel served all the guests for dinner. “The flavour of the Chanterelle mushrooms and the fragrance of the tiny alpine strawberries was unforgettable. I was hooked from that day on.”'),
(10, '', 'http://localhost/Itemimage/aboutus/', 'Steve did a commercial cookery course and a three year apprenticeship at The Beach House Restaurant at Whale Beach, Sydney prior to starting Paradise Beach Purveyors with Anmarie.');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`) VALUES
(1, 'admin@gmail.com', 'password');

-- --------------------------------------------------------

--
-- Table structure for table `awards`
--

CREATE TABLE `awards` (
  `id` int(11) NOT NULL,
  `filename` varchar(250) NOT NULL,
  `imageurl` varchar(500) NOT NULL,
  `awardyear` date DEFAULT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `awards`
--

INSERT INTO `awards` (`id`, `filename`, `imageurl`, `awardyear`, `description`) VALUES
(1, 'award-silver21.gif', 'http://localhost/Itemimage/award/award-silver21.gif', '2012-05-11', 'Tzatziki - Silver Medal <br>\r\nSydney Royal Cheese & Dairy Produce Show<br>\r\nMost Best Successful Dip Exhibitor'),
(2, 'ChampionAward.gif', 'http://localhost/Itemimage/award/ChampionAward.gif', '2011-05-10', 'Pesto Swirl - Champion product<br>\r\nSydney Royal Cheese & Dairy Produce Show<br>\r\nMost Best Successful Dip Exhibitor'),
(4, 'award-gold.gif', 'http://localhost/Itemimage/award/award-gold.gif', '2011-05-11', 'Pesto Swirl - Gold Medal<br>\r\nSydney Royal Cheese & Dairy Produce Show<br>\r\nMost Best Successful Dip Exhibitor'),
(5, 'award-silver1.gif', 'http://localhost/Itemimage/award/award-silver1.gif', '2011-05-11', 'Smoked Trout & Chives - Silver Medal<br>\r\nSydney Royal Cheese & Dairy Produce Show<br>\r\nMost Best Successful Dip Exhibitor'),
(6, 'award-silver2.gif', 'http://localhost/Itemimage/award/award-silver2.gif', '2011-05-11', 'Smoked Salmon & Dill - Silver Medal<br>\r\nSydney Royal Cheese & Dairy Produce Show<br>\r\nMost Best Successful Dip Exhibitor');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `imageurl` varchar(500) NOT NULL,
  `description` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `image`, `imageurl`, `description`) VALUES
(1, 'food-2608571_1920.jpg', 'http://localhost/Itemimage/banner/food-2608571_1920.jpg', 'Savour our range of<br> healthy homemade dips'),
(3, 'starters-2157242_1920.jpg', 'http://localhost/Itemimage/banner/starters-2157242_1920.jpg', 'Winter is done!<br>prepare your taste bud with<br>\r\nour new seasonal dips!'),
(4, 'dip-banner.jpg', 'http://localhost/Itemimage/banner/dip-banner.jpg', 'Excellent experience!');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `categoryname` varchar(100) NOT NULL,
  `categoryid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `categoryname`, `categoryid`) VALUES
(6, 'Pitabits', NULL),
(11, 'Gluten Free', NULL),
(12, 'Paradise Beach', NULL),
(13, 'No Added Sugar', NULL),
(14, 'Vegetarian', NULL),
(16, 'Dips', NULL),
(17, 'Michael Fine Foods', NULL),
(18, 'LIT', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `contactno` varchar(250) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `contactno`, `address`, `email`) VALUES
(1, 'Kitchen: + 61 2 9907 8056<br>Steve Ingram Mobile: 0413 316 709', 'Paradise Beach Purveyors<br>\r\nUnit 13/2 Paton Place<br>\r\nBalgowlah NSW 2093 <br>\r\nAustralia ', 'Email: orders@paradisebeach.info');

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE `home` (
  `id` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `subtitle` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `imageurl` varchar(500) DEFAULT NULL,
  `link` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home`
--

INSERT INTO `home` (`id`, `title`, `subtitle`, `image`, `imageurl`, `link`) VALUES
(1, 'Our Story', 'Jude\'s Husband Had A Dream', '597.jpg', 'http://localhost/Itemimage/home/597.jpg', 'http://localhost/index.php/home/products/4'),
(3, 'Our Flavours', 'What is your favourite?', '991.png', 'http://localhost/Itemimage/home/991.png', NULL),
(5, 'Salted Caramel', 'Read The Story', '597.jpg', 'http://localhost/Itemimage/home/597.jpg', NULL),
(6, 'Trial', 'Sample 5', 'banner_chicken_mcnugget.jpg', 'http://localhost/Itemimage/home/banner_chicken_mcnugget.jpg', ''),
(7, 'Trial', 'LIT', 'banner_chicken_mcnugget.jpg', 'http://localhost/Itemimage/home/banner_chicken_mcnugget.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `productawards`
--

CREATE TABLE `productawards` (
  `id` int(11) NOT NULL,
  `productid` int(11) DEFAULT NULL,
  `awardid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productawards`
--

INSERT INTO `productawards` (`id`, `productid`, `awardid`) VALUES
(6, 4, 2),
(7, 4, 4),
(8, 9, 1),
(9, 10, 6);

-- --------------------------------------------------------

--
-- Table structure for table `productcategory`
--

CREATE TABLE `productcategory` (
  `id` int(11) NOT NULL,
  `productid` int(11) DEFAULT NULL,
  `categoryid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productcategory`
--

INSERT INTO `productcategory` (`id`, `productid`, `categoryid`) VALUES
(18, 4, 11),
(20, 4, 12),
(31, 9, 16),
(32, 9, 12),
(33, 9, 13),
(34, 9, 11),
(35, 9, 14),
(36, 4, 16),
(37, 4, 14),
(38, 10, 16),
(39, 10, 13),
(40, 10, 12),
(41, 10, 11),
(42, 11, 6),
(43, 11, 12),
(44, 11, 14),
(45, 12, 17),
(46, 12, 16),
(47, 12, 11),
(48, 12, 13),
(49, 12, 14),
(50, 4, 13);

-- --------------------------------------------------------

--
-- Table structure for table `productrecipesuggestion`
--

CREATE TABLE `productrecipesuggestion` (
  `id` int(11) NOT NULL,
  `productid` int(11) DEFAULT NULL,
  `recipeid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productrecipesuggestion`
--

INSERT INTO `productrecipesuggestion` (`id`, `productid`, `recipeid`) VALUES
(9, 9, 3),
(10, 10, 4),
(11, 12, 5),
(12, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `productname` varchar(100) NOT NULL,
  `productimage` varchar(250) DEFAULT NULL,
  `imageurl` varchar(500) DEFAULT NULL,
  `nutritionfile` varchar(200) DEFAULT NULL,
  `nutritionfileurl` varchar(500) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `ingredients` varchar(500) DEFAULT NULL,
  `allergens` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `productname`, `productimage`, `imageurl`, `nutritionfile`, `nutritionfileurl`, `description`, `ingredients`, `allergens`) VALUES
(4, 'Pesto Swirl', 'PestoSwirl1.jpg', 'http://localhost/Itemimage/product/PestoSwirl1.jpg', 'img_nutri-smoked-salmon-dill6.gif', 'http://localhost/Itemimage/nutrition/img_nutri-smoked-salmon-dill6.gif', 'The contrasting texture of an elegant lemon and cracked pepper sour cream base swirls together with the bright colours and intensely aromatic flavours of our handmade Classic Basil and Char grilled Red Pepper Pesto’s. To enjoy this combination you can either mix it all together and eat it as it is, or turn it out onto a plate for a colourful, vibrant and enticing dip. Gluten Free.', 'Sour cream (pasteurised cream, culture), cream cheese [milk, cream, salt, vegetable gums (410, 412,415), culture], sunflower oil, lemon juice [reconstituted lemon juice, Vitamin C, preservatives (211,223)], red pepper (4.6%), parmesan cheese (3.9%), basil puree (1.8%), cashews (1.8%), olive oil, garlic (1.5%), salt, parsley, pine nuts (0.5%), spices,\r\nhorseradish, tomato paste, preservative (211).\r\nProduced on the same line as products with othpeanuts, sesame seeds, fish, crustacea.', 'Nuts and Dairy <br><br>\r\n*Produced on the same line as products with sesame seeds, milk, nuts, peanuts, eggs, fish and crustacea.'),
(9, 'Tzatziki', 'Tzatziki.jpg', 'http://localhost/Itemimage/product/Tzatziki.jpg', 'nutri-dips-tzatziki.gif', 'http://localhost/Itemimage/nutrition/nutri-dips-tzatziki.gif', 'Creamy premium yoghurt, combined with coarsely shredded cucumber, roughly chopped fresh mint, fresh garlic and olive oil. We think it\'s as much a salad as it is a dip, bursting with natural flavour and textures.', 'Yoghurt (74%) (cows milk, skim milk powder, culture), cucumber, sunflower oil, vinegar, garlic, salt, extra virgin olive oil, mint, spices, preservative (211). ', 'Dairy, Garlic and Oil <br><br>\r\n* Produced on the same line as products with nuts, peanuts, sesame seeds, fish and crustacea.'),
(10, 'Smoked Salmon & Dill', 'SmokedSalmonDillDip.jpg', 'http://localhost/Itemimage/product/SmokedSalmonDillDip.jpg', 'nutri-dips-smoked-salmon-dill.gif', 'http://localhost/Itemimage/nutrition/nutri-dips-smoked-salmon-dill.gif', 'Stunningly aromatic fresh dill and a hint of capers marry simply and elegantly with premium Tasmanian hot smoked salmon, cream cheese and sour cream to create a classic.', 'Sour cream (pasteurised cream, culture), cream cheese [milk, cream, salt, vegetable gums (410,412,415), culture], smoked salmon (26%) (salmon, salt, natural wood smoke), lemon juice [reconstituted lemon juice, Vitamin C, preservatives (211,223)], capers, dill (2%), onion, spices, salt, preservative (211). ', 'Fish and Dairy.<br><br>\r\n* Produced on the same line as products with nuts, peanuts, sesame seeds, crustacea.'),
(11, 'Original Pita Bits', 'OriginalPitaBits.jpg', 'http://localhost/Itemimage/product/OriginalPitaBits.jpg', 'nutri-pbp-original-pita-bits.JPG', 'http://localhost/Itemimage/nutrition/nutri-pbp-original-pita-bits.JPG', 'Our Original Pita Bits are made crisp and golden bite size with a nutty, neutral, toasty flavour that’s an instant hit with everyone who tastes them. Perfect with dips, drinks, appetisers, antipasto, mezze plates, pates and cheeses. Pita Bits are the ideal partner for all your favourite savoury treats.', 'Wheat flour, Salt, yeast, sugar, water added.', 'Gluten.<br><br>\r\n* Produced on the same line as products with nuts, peanuts, sesame seeds, crustacea.'),
(12, 'Hommos', 'MFF_Hommos_200g1.jpg', 'http://localhost/Itemimage/product/MFF_Hommos_200g1.jpg', 'nutri-dips-hommus.gif', 'http://localhost/Itemimage/nutrition/nutri-dips-hommus.gif', 'Leaning towards the traditional, homemade style nutty texture, premium tahini, extra virgin olive oil, fresh garlic, plenty of lemon juice, a hint of cumin as recommended by many Middle Eastern cooks', 'Chickpeas (52%), lemon juice [reconstituted lemon juice, Vitamin C, preservatives (211,223)], tahini (sesame seeds),  sunflower oil, extra virgin olive oil, spices, garlic, onion, salt, preservative (211). ', 'Sesame, Garlic, Onions and Oil<br><br>\r\n* Produced on the same line as products with milk, nuts, peanuts, fish, crustacea');

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

CREATE TABLE `recipe` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `filename` varchar(250) NOT NULL,
  `imageurl` varchar(500) NOT NULL,
  `ingredient` varchar(500) NOT NULL,
  `recipe_procedure` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recipe`
--

INSERT INTO `recipe` (`id`, `title`, `filename`, `imageurl`, `ingredient`, `recipe_procedure`) VALUES
(3, 'Sumac Lamb Kebabs with Tzatziki', 'pbp_recipe_tzatziki_1.jpg', 'http://localhost/Itemimage/recipe/pbp_recipe_tzatziki_1.jpg', '1 tub of Paradise Beach Purveyors Tzatziki <br>\r\n1 large red onion, peeled <br>\r\n1 large red capsicum, de-seeded <br>\r\n800g lamb fillet <br>\r\n12 skewers <br>\r\n2 tablespoons sumac spice <br>\r\n2 tablespoons vegetable oil <br>', 'Cut onion and capsicum into bite-sized squares and the lamb into bite-sized chunks. Thread alternate pieces of onion, lamb and capsicum onto the skewers and roll in sumac spice. <br>\r\nHeat the oil in a frying pan and brown the skewers, turning from time to time until the lamb is cooked to your liking. You can also cook these on the BBQ. Serve with Paradise Beach Purveyors creamy Tzatziki. <br>\r\nServes 4.'),
(4, 'Smoked Salmon & Dill Bites', 'pbp_recipe_smokedsalmondill.jpg', 'http://localhost/Itemimage/recipe/pbp_recipe_smokedsalmondill.jpg', '1 tub Paradise Beach Purveyors Smoked Salmon & Dill Dip <br>\r\n250g pack of sliced smoked salmon <br>\r\n1 bunch of English spinach <br>\r\n1 pack of sliced pumpernickel <br>\r\nDill tips for garnish <br>', 'Spread a layer of cling film on a clean surface such as a chopping board.\r\nWash and trim the spinach and place in a heatproof bowl of boiling water to wilt for a minute. Remove and drain. <br>\r\nLay smoked salmon slices across the cling film in a line to form a base, approximately 20cm long. Next cover the salmon with the wilted baby spinach, and spread with a 2cm layer of Paradise Beach Purveyors Smoked Salmon and Dill Dip.'),
(5, 'Pizza with Hommus, Olives, Tomato and Goat Fetta', 'mff_recipe_hommus.jpg', 'http://localhost/Itemimage/recipe/mff_recipe_hommus.jpg', '1 tub of Paradise Beach Purveyors Classic Hommus<br>\r\n1 good quality pizza base<br>\r\n½ punnet cherry tomatoes<br>\r\n1 small jar of marinated artichoke hearts<br>\r\n12 medium Kalamata olives<br>\r\n½ red onion, peeled and sliced<br>\r\n100g fresh goat fetta cheese<br>\r\nHandful of baby rocket leaves', 'Heat the oven to 200˚C.<br>\r\nPlace the pizza base on a pizza tray and cover the base with generous amounts of Paradise Beach Purveyors Classic Hommus.<br>\r\nSlice the cherry tomatoes and artichokes in half then top the pizza base with the red onion, artichoke hearts, Kalamata olives and halved cherry tomatoes.<br>\r\nCrumble the goat fetta on top and place in the oven to cook for approximately 10 minutes, or until the edges of the pizza are crisp.<br>\r\nRemove from oven and add rockets.');

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(500) DEFAULT NULL,
  `district` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `post_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`id`, `name`, `address`, `district`, `state`, `post_code`) VALUES
(1, 'Food Corner', '48 Pirrama Rd, Pyrmont, NSW, Australia', 'Sydney', 'NSW', '1212'),
(2, 'Coles Supermarkets', '26 Howard Ave, Dee Why, NSW 2099, Australia', 'Sydney', 'NSW', '2099');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `email` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`) VALUES
(2, 'amd690544@gmail.com'),
(3, 'rbs@gmail.com'),
(4, 'me@domain.com'),
(5, 'user@user.com'),
(6, 'me@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `image` varchar(500) DEFAULT NULL,
  `imageurl` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `name`, `description`, `image`, `imageurl`) VALUES
(1, 'Barak', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. !', 'barack-obama-12782369-1-402.jpg', 'http://localhost/Itemimage/testimonial/barack-obama-12782369-1-402.jpg'),
(2, 'Someone famous', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. !', '128.jpg', 'http://localhost/Itemimage/testimonial/128.jpg'),
(3, 'Someone famous', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. !', '128.jpg', 'http://localhost/Itemimage/testimonial/128.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `awards`
--
ALTER TABLE `awards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoryid` (`categoryid`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productawards`
--
ALTER TABLE `productawards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productid` (`productid`),
  ADD KEY `awardid` (`awardid`);

--
-- Indexes for table `productcategory`
--
ALTER TABLE `productcategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productid` (`productid`),
  ADD KEY `categoryid` (`categoryid`);

--
-- Indexes for table `productrecipesuggestion`
--
ALTER TABLE `productrecipesuggestion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productid` (`productid`),
  ADD KEY `recipeid` (`recipeid`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `awards`
--
ALTER TABLE `awards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home`
--
ALTER TABLE `home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `productawards`
--
ALTER TABLE `productawards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `productcategory`
--
ALTER TABLE `productcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `productrecipesuggestion`
--
ALTER TABLE `productrecipesuggestion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `recipe`
--
ALTER TABLE `recipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`categoryid`) REFERENCES `category` (`id`);

--
-- Constraints for table `productawards`
--
ALTER TABLE `productawards`
  ADD CONSTRAINT `productawards_ibfk_1` FOREIGN KEY (`productid`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `productawards_ibfk_2` FOREIGN KEY (`awardid`) REFERENCES `awards` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `productcategory`
--
ALTER TABLE `productcategory`
  ADD CONSTRAINT `productcategory_ibfk_1` FOREIGN KEY (`productid`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `productcategory_ibfk_2` FOREIGN KEY (`categoryid`) REFERENCES `category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `productrecipesuggestion`
--
ALTER TABLE `productrecipesuggestion`
  ADD CONSTRAINT `productrecipesuggestion_ibfk_1` FOREIGN KEY (`productid`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `productrecipesuggestion_ibfk_2` FOREIGN KEY (`recipeid`) REFERENCES `recipe` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
